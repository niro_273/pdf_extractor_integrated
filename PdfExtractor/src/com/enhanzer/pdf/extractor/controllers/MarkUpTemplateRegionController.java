package com.enhanzer.pdf.extractor.controllers;

import com.enhanzer.pdf.extractor.model.data.markup.template.MarkUpResponse;
import com.enhanzer.pdf.extractor.model.template.markup.MarkUpRequestProcessor;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class MarkUpTemplateRegionController extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        MarkUpRequestProcessor requestProcessor=new MarkUpRequestProcessor();

        StringBuilder sb = new StringBuilder();
        String s;
        while ((s = request.getReader().readLine()) != null) {
            sb.append(s);
        }

        /*
        Process Request and Extract Data or Insert Data and return the response
         */
        MarkUpResponse markUpResponse=requestProcessor.processRequest(sb.toString());

        Gson gson = new Gson();
        response.getWriter().print(gson.toJson(markUpResponse));
    }
}
