package com.enhanzer.pdf.extractor.controllers;

import com.enhanzer.pdf.extractor.model.data.form.populate.FormPopulateData;
import com.enhanzer.pdf.extractor.model.form.populate.MainCategories;
import com.enhanzer.pdf.extractor.model.form.populate.SubCategories;
import com.enhanzer.pdf.extractor.model.form.populate.Templates;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FormPopulateController extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        StringBuilder sb = new StringBuilder();
        String s;
        while ((s = request.getReader().readLine()) != null) {
            sb.append(s);
        }
        Gson gson=new Gson();

        FormPopulateData formPopulateData;
        formPopulateData=gson.fromJson(sb.toString(),FormPopulateData.class);

        if(formPopulateData.getRequest().equals("getMainCategories")){
            MainCategories mainCategories=new MainCategories();
            mainCategories.getMainCategories(formPopulateData);

        }
        if(formPopulateData.getRequest().equals("getSubCategories")){
            SubCategories subCategories=new SubCategories();
            subCategories.getSubCategories(formPopulateData);
        }

        if(formPopulateData.getRequest().equals("getTemplates")){
            Templates templates=new Templates();
            templates.getTemplates(formPopulateData);
        }
        response.getWriter().print(gson.toJson(formPopulateData));
    }


}
