package com.enhanzer.pdf.extractor.controllers;

import com.enhanzer.pdf.extractor.model.data.extract.pdf.ExtractResponse;
import com.enhanzer.pdf.extractor.model.data.extract.pdf.ExtractStatus;
import com.enhanzer.pdf.extractor.model.extract.pdf.RequestProcessor;
import com.enhanzer.pdf.extractor.model.extract.pdf.ResponseGenerator;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;


public class ExtractPdfController extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestProcessor requestProcessor = new RequestProcessor();
        ExtractStatus extractStatus = new ExtractStatus();
        extractStatus.setRootPath(getServletContext().getRealPath(File.separator));

        requestProcessor.processRequest(request, extractStatus);

        ResponseGenerator responseGenerator=new ResponseGenerator();
        Gson gson=new Gson();
        ExtractResponse extractResponse;
        extractResponse=responseGenerator.generateResponse(extractStatus);

        response.getWriter().print(gson.toJson(extractResponse));

    }
}
