package com.enhanzer.pdf.extractor.controllers;

import com.enhanzer.pdf.extractor.model.data.doc.text.positions.TextData;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.util.PDFTextStripper;
import org.apache.pdfbox.util.TextPosition;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PrintTextLocations extends PDFTextStripper
{
    public static List<TextData> textDatas=new ArrayList<TextData>();
    /**
     * Default constructor.
     *
     * @throws java.io.IOException If there is an error loading text stripper properties.
     */
    public PrintTextLocations() throws IOException
    {
        super.setSortByPosition( true );
    }

    /**
     * This will print the documents data.
     *
     * @param args The command line arguments.
     *
     * @throws Exception If there is an error parsing the document.
     */
    public static void main( String[] args ) throws Exception
    {

        PDDocument document = null;
        try
        {
            document = PDDocument.load( new File("/home/niro273/brandix_doc_mining/documents/18Garment Spec.pdf") );
            if( document.isEncrypted() )
            {
                document.decrypt( "" );
            }
            PrintTextLocations printer = new PrintTextLocations();
            List allPages = document.getDocumentCatalog().getAllPages();

            for( int i=0; i<1; i++ )
            {
                PDPage page = (PDPage)allPages.get( i );
                System.out.println( "Processing page: " + i );
                PDStream contents = page.getContents();
                if( contents != null )
                {
                    printer.processStream( page, page.findResources(), page.getContents().getStream() );
                }
            }
        }
        finally
        {
            if( document != null )
            {
                document.close();
            }
        }
        System.out.println(textDatas.get(1360).getCharacter());
        System.out.println(textDatas.get(1361).getCharacter());
        System.out.println(textDatas.get(1362).getCharacter());
        System.out.println(textDatas.get(1363).getCharacter());

    }


    /**
     * A method provided as an event interface to allow a subclass to perform
     * some specific functionality when text needs to be processed.
     *
     * @param text The text to be processed
     */
    protected void processTextPosition( TextPosition text )
    {

        TextData textData=new TextData();
        textData.setCharacter(text.getCharacter());
        textData.setxDirAdj(text.getXDirAdj());
        textData.setyDirAdj(text.getYDirAdj());
        textData.setFontSize(text.getFontSize());
        textData.setxScale(text.getXScale());
        textData.setHeightDir(text.getHeightDir());
        textData.setWidthOfSpace(text.getWidthOfSpace());
        textData.setWidthDirAdj(text.getWidthDirAdj());

        textDatas.add(textData);

        System.out.println( "String[" + text.getXDirAdj() + "," +
                text.getYDirAdj() + " fs=" + text.getFontSize() + " xscale=" +
                text.getXScale() + " height=" + text.getHeightDir() + " space=" +
                text.getWidthOfSpace() + " width=" +
                text.getWidthDirAdj() + "]" + text.getCharacter() );
    }

    /**
     * This will print the usage for this document.
     */
    private static void usage()
    {
        System.err.println( "Usage: java org.apache.pdfbox.examples.pdmodel.PrintTextLocations <input-pdf>" );
    }

}
