package com.enhanzer.pdf.extractor.controllers;

import com.enhanzer.pdf.extractor.model.data.manage.categories.ManageCategoriesData;
import com.enhanzer.pdf.extractor.model.manage.categories.RequestProcessor;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class ManageCategoriesController extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        StringBuilder sb = new StringBuilder();
        String s;
        while ((s = request.getReader().readLine()) != null) {
            sb.append(s);
        }
        Gson gson = new Gson();

        ManageCategoriesData manageCategoriesData;
        manageCategoriesData=gson.fromJson(sb.toString(),ManageCategoriesData.class);


        RequestProcessor requestProcessor=new RequestProcessor();
        requestProcessor.processRequest(manageCategoriesData);


        response.getWriter().print(gson.toJson(manageCategoriesData));



    }
}
