package com.enhanzer.pdf.extractor.model.complex.table;

import com.enhanzer.pdf.extractor.model.data.extract.pdf.ExtractStatus;
import com.enhanzer.pdf.extractor.model.data.template.info.table.Column;
import com.enhanzer.pdf.extractor.model.data.template.info.table.TableDataElement;
import com.enhanzer.pdf.extractor.model.data.template.info.table.TableDataParser;
import com.enhanzer.pdf.extractor.model.extract.pdf.inserter.DepExtractedTableInserter;
import com.enhanzer.pdf.extractor.model.extract.pdf.retriever.TableDataRetriever;
import com.enhanzer.pdf.extractor.model.extractors.tabel.TableExtractor;
import org.apache.pdfbox.pdmodel.PDDocument;

import java.io.IOException;
import java.util.List;

public class ProcessTable {
    public static void main(String args[]){

    }

    public void getTableDataElements(ExtractStatus extractStatus) throws IOException {
        TableDataRetriever tableDataRetriever=new TableDataRetriever();
        TableDataParser tableDataParser=tableDataRetriever.retrieveTableData(extractStatus);

        TableExtractor tableExtractor=new TableExtractor();
        List<TableDataElement> tableDataElements=tableDataParser.getTableDataElements();
        PDDocument doc=PDDocument.load(extractStatus.getUploadedPdfFile());

        for(TableDataElement ta:tableDataElements) {
            tableExtractor.extract(doc, ta);
            List<Column> columns=ta.getColumns();
            Column column=columns.get(0);
            column.getExtractedValues();
        }

        DepExtractedTableInserter tableInserter=new DepExtractedTableInserter();
        tableInserter.insert(tableDataParser,extractStatus);

    }
}
