package com.enhanzer.pdf.extractor.model.data.extract.pdf;

/**
 * Created by niro273 on 9/9/14.
 */
public class ExtractResponse {
    private boolean status;
    private String errorCause;

    public String getSuccessMsg() {
        return successMsg;
    }

    public void setSuccessMsg(String successMsg) {
        this.successMsg = successMsg;
    }

    public String getErrorCause() {
        return errorCause;
    }

    public void setErrorCause(String errorCause) {
        this.errorCause = errorCause;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    private String successMsg;
}
