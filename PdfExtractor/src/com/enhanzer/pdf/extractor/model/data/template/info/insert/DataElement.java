package com.enhanzer.pdf.extractor.model.data.template.info.insert;

import javax.annotation.Generated;

import com.enhanzer.pdf.extractor.model.data.template.info.image.ImageDataParser;
import com.enhanzer.pdf.extractor.model.data.template.info.table.TableDataParser;
import com.enhanzer.pdf.extractor.model.data.template.info.text.TextDataParser;
import com.google.gson.annotations.Expose;

@Generated("org.jsonschema2pojo")

public class DataElement {
    //@Expose
    //private ImageDataParser imageDataParser;

    public TextDataParser getTextDataParser() {
        return textDataParser;
    }

    public void setTextDataParser(TextDataParser textDataParser) {
        this.textDataParser = textDataParser;
    }

    @Expose
    private TextDataParser textDataParser;

    //@Expose
    //private TableDataParser tableDataParser;

}
