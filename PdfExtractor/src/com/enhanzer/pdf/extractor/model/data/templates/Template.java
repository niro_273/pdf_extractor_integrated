package com.enhanzer.pdf.extractor.model.data.templates;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("org.jsonschema2pojo")
public class Template {

    @Expose
    private String templateName;
    @Expose
    private String pdfFile;

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getPdfFile() {  return pdfFile;   }

    public void setPdfFile(String pdfFile) {  this.pdfFile = pdfFile;  }

}