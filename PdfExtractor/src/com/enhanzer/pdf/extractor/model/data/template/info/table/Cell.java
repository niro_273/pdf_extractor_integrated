package com.enhanzer.pdf.extractor.model.data.template.info.table;


public class Cell {

    private StringBuilder value;
    private float xPosFirst;
    private float yPosFirst;
    private float xPosLast;
    private float yPostLast;

    public  Cell(){
        value=new StringBuilder("");
    }
    public StringBuilder getValue() {        return value;    }

    public void setValue(StringBuilder value) {        this.value = value;    }

    public float getxPosFirst() {        return xPosFirst;    }

    public void setxPosFirst(float xPosFirst) {        this.xPosFirst = xPosFirst;    }

    public float getyPosFirst() {        return yPosFirst;    }

    public void setyPosFirst(float yPosFirst) {        this.yPosFirst = yPosFirst;    }

    public float getxPosLast() {        return xPosLast;    }

    public void setxPosLast(float xPosLast) {        this.xPosLast = xPosLast;    }

    public float getyPostLast() {        return yPostLast;    }

    public void setyPostLast(float yPostLast) {        this.yPostLast = yPostLast;    }


}
