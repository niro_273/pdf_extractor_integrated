package com.enhanzer.pdf.extractor.model.form.populate;

import com.enhanzer.pdf.extractor.model.data.form.populate.FormPopulateData;
import com.enhanzer.pdf.extractor.model.db.connect.MongoDB;
import com.mongodb.*;

import java.net.UnknownHostException;
import java.util.List;

public class MainCategories {

    /*
    Templates Collection Data Format :
    {mainCategory:'mainCategory',subCategory:'subCategory',templates:[templateName:'name1']}
    */
    private static final String host="localhost";
    private static final String dbName="staging";
    private static final String templatesColl="templates";
    private static final int    port=27017;


    public  FormPopulateData getMainCategories(FormPopulateData formPopulateData){

        try {
            MongoDB mongoDB=new MongoDB();
            DB mongoClient = mongoDB.createMongoClient(host, dbName, port);
            DBCollection templateCollection = mongoDB.getCollection(mongoClient,templatesColl);
            /*
            Returns a array of distinct main categories
             */
            List list= templateCollection.distinct("mainCategory");
            String[] mainCategories=new String[list.size()];
            for(int i=0 ; i<list.size();i++){
                mainCategories[i]= (String) list.get(i);
            }

            formPopulateData.setMainCategories(mainCategories);
        }catch (UnknownHostException e){
            e.printStackTrace();
        }
        return formPopulateData;
    }
}
