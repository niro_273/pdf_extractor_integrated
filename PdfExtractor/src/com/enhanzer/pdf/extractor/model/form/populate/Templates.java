package com.enhanzer.pdf.extractor.model.form.populate;

import com.enhanzer.pdf.extractor.model.data.form.populate.FormPopulateData;
import com.enhanzer.pdf.extractor.model.data.templates.Template;
import com.enhanzer.pdf.extractor.model.data.templates.TemplatesParser;
import com.enhanzer.pdf.extractor.model.db.connect.MongoDB;
import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

import java.net.UnknownHostException;
import java.util.List;


public class Templates {
    /*
    Templates Collection Data Format :
    {mainCategory:'mainCategory',subCategory:'subCategory',templates:[templateName:'name1']}
    */
    private static final String host="localhost";
    private static final String dbName="staging";
    private static final String templatesColl="templates";
    private static final int    port=27017;

    public FormPopulateData getTemplates(FormPopulateData formPopulateData){

        try {
            TemplatesParser templatesParser;
            MongoDB mongoDB=new MongoDB();
            DB mongoClient = mongoDB.createMongoClient(host,dbName,port);
            DBCollection templateCollection = mongoDB.getCollection(mongoClient,templatesColl);
            BasicDBObject basicDBObject = new BasicDBObject();
            /*
            Returns the DB cursor object where the mainCategory=form.mainCategory
             */
            basicDBObject.put("mainCategory", formPopulateData.getSelectedMC());
            basicDBObject.put("subCategory", formPopulateData.getSelectedSC());
            DBCursor templateCursor = templateCollection.find(basicDBObject);


            if(templateCursor.hasNext()){
                // If there is a Record Present parse the MongoObject returned
                Gson gson = new Gson();
                templatesParser = gson.fromJson(templateCursor.next().toString()
                        ,TemplatesParser.class);
                List<Template> templates=templatesParser.getTemplates();

                String[] templateNames=new String[templates.size()];

                for(int i=0; i<templates.size();i++){
                    templateNames[i]=templates.get(i).getTemplateName();
                }

                formPopulateData.setTemplateNames(templateNames);
            }
        }catch (UnknownHostException e){
            e.printStackTrace();
        }

        return formPopulateData;
    }

}
