package com.enhanzer.pdf.extractor.model.form.populate;

import com.enhanzer.pdf.extractor.model.data.form.populate.FormPopulateData;
import com.enhanzer.pdf.extractor.model.data.templates.TemplatesParser;
import com.enhanzer.pdf.extractor.model.db.connect.MongoDB;
import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

import java.net.UnknownHostException;


public class SubCategories {

    /*
    Templates Collection Data Format :
    {mainCategory:'mainCategory',subCategory:'subCategory',templates:[templateName:'name1']}
    */
    private static final String host="localhost";
    private static final String dbName="staging";
    private static final String templatesColl="templates";
    private static final int    port=27017;

    public FormPopulateData getSubCategories(FormPopulateData formPopulateData){

        try {

            MongoDB mongoDB=new MongoDB();
            DB mongoClient = mongoDB.createMongoClient(host,dbName,port);
            DBCollection templateCollection = mongoDB.getCollection(mongoClient,templatesColl);
            BasicDBObject basicDBObject = new BasicDBObject();
            /*
            Returns the DB cursor object where the mainCategory=form.mainCategory
             */
            basicDBObject.put("mainCategory", formPopulateData.getSelectedMC());
            DBCursor templateCursor = templateCollection.find(basicDBObject);

            String[] subCategories=new String[templateCursor.size()];
            int i=0;
            while (templateCursor.hasNext()){
                // If there is a Record Present parse the MongoObject returned
                Gson gson = new Gson();
                TemplatesParser templatesParser = gson.fromJson(templateCursor.next().toString()
                        ,TemplatesParser.class);
                subCategories[i]=templatesParser.getSubCategory();
                i++;
            }

            formPopulateData.setSubCategories(subCategories);

        }catch (UnknownHostException e){
            e.printStackTrace();
        }

        return formPopulateData;
    }
}
