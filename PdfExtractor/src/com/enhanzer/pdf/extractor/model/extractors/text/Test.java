package com.enhanzer.pdf.extractor.model.extractors.text;


import com.enhanzer.pdf.extractor.model.data.template.info.text.TextDataParser;
import com.enhanzer.pdf.extractor.model.db.connect.MongoDB;
import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

import java.net.UnknownHostException;


public class Test {
    private static final String host = "localhost";
    private static final String dbName = "staging";
    private static final String templatesColl = "templateInfo";
    private static final int port = 27017;

    public static void main(String args[]){

        testMongo();
    }

    public static void testMongo() {
        try {

            MongoDB mongoDB = new MongoDB();
            DB mongoClient = mongoDB.createMongoClient(host, dbName, port);
            DBCollection templatesCollection = mongoDB.getCollection(mongoClient, templatesColl);

            BasicDBObject searchQuery = new BasicDBObject();
            searchQuery.put("mainCategory", "Sales Order");
            searchQuery.put("subCategory", "Supplier 10");
            searchQuery.put("templateName","templatec");
            searchQuery.put("dataType","table");

            DBCursor templateCursor = templatesCollection.find(searchQuery);

            if (templateCursor.hasNext()){
                //System.out.println(templateCursor.next().toString());
                Gson gson=new Gson();
                String s=templateCursor.next().toString();
                System.out.println(s);
                TextDataParser imageDataParser=gson.fromJson(s,TextDataParser.class);
                imageDataParser.getMainCategory();
                imageDataParser.getSubCategory();
            }



        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}