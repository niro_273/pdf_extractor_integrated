package com.enhanzer.pdf.extractor.model.login;

import com.enhanzer.pdf.extractor.model.data.authenticate.login.LoginRequest;
import com.enhanzer.pdf.extractor.model.data.templates.Template;
import com.enhanzer.pdf.extractor.model.data.templates.TemplatesParser;
import com.enhanzer.pdf.extractor.model.data.upload.template.UploadStatus;
import com.enhanzer.pdf.extractor.model.db.connect.MongoDB;
import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import org.apache.commons.logging.Log;

import java.net.UnknownHostException;
import java.util.List;

public class Authenticator {

    /*
    users Collection Data Format :
    {'userName':'userName',pass:'pass','org':'brandix'}
     */
    private static final String host="localhost";
    private static final String dbName="staging";
    private static final String usersColl="users";
    private static final int    port=27017;


    public LoginRequest authenticate(LoginRequest loginRequest){

        try {

            MongoDB mongoDB=new MongoDB();
            loginRequest.setIsAuthenticated(false);
            DB mongoClient = mongoDB.createMongoClient(host,dbName,port);
            DBCollection usersCollection = mongoDB.getCollection(mongoClient,usersColl);
            BasicDBObject basicDBObject = new BasicDBObject();

            /*
            Returns the DB cursor object where the mainCategory=form.mainCategory and
            subCategory=form.subCategory from the templates Collections
             */
            basicDBObject.put("userName", loginRequest.getUserName());
            basicDBObject.put("pass", loginRequest.getPass());
            DBCursor templateCursor = usersCollection.find(basicDBObject);

            if (templateCursor.size()==1){
                loginRequest.setIsAuthenticated(true);
            }else {
                loginRequest.setErrorCause("Invalid UserName or Password");
            }
        }catch (UnknownHostException e){
            e.printStackTrace();
        }
        return loginRequest;
    }
}
