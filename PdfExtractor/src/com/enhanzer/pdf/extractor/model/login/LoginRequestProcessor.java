package com.enhanzer.pdf.extractor.model.login;


import com.enhanzer.pdf.extractor.model.data.authenticate.login.LoginRequest;
import com.enhanzer.pdf.extractor.model.data.authenticate.login.LoginResponse;

public class LoginRequestProcessor {

    public LoginRequest processRequest(LoginRequest loginRequest){

        Authenticator authenticator=new Authenticator();
        authenticator.authenticate(loginRequest);

        return loginRequest;

    }

    public LoginResponse generateResponse(LoginRequest loginRequest){

        LoginResponse loginResponse=new LoginResponse();
        loginResponse.setIsAuthenticated(loginRequest.getIsAuthenticated());
        loginResponse.setErrorCause(loginRequest.getErrorCause());

        return loginResponse;
    }
}
