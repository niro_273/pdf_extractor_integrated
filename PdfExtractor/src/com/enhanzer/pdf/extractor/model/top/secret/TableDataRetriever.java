package com.enhanzer.pdf.extractor.model.top.secret;


import com.enhanzer.pdf.extractor.model.data.extract.pdf.ExtractStatus;
import com.enhanzer.pdf.extractor.model.data.template.info.table.TableDataParser;
import com.enhanzer.pdf.extractor.model.db.connect.MongoDB;
import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

import java.net.UnknownHostException;

public class TableDataRetriever {

    private static final String host="localhost";
    private static final String dbName="staging";
    private static final String templatesColl="templateInfo";
    private static final int    port=27017;


    public TableDataParser retrieveTableData(ExtractStatus extractStatus){
        TableDataParser tableDataParser=null;
        try {

            MongoDB mongoDB=new MongoDB();
            DB mongoClient = mongoDB.createMongoClient(host,dbName,port);
            DBCollection templateCollection = mongoDB.getCollection(mongoClient,templatesColl);
            BasicDBObject basicDBObject = new BasicDBObject();
            /*
            Returns the DB cursor object where the mainCategory=form.mainCategory and
            subCategory=form.subCategory from the templates Collections
             */
            basicDBObject.put("mainCategory", extractStatus.getMainCategory());
            basicDBObject.put("subCategory", extractStatus.getSubCategory());
            basicDBObject.put("templateName", extractStatus.getTemplateName());
            basicDBObject.put("dataType", "table");
            DBCursor templateCursor = templateCollection.find(basicDBObject);

            if (templateCursor.hasNext()){
                // If there is a Record Present parse the MongoObject returned
                Gson gson = new Gson();
                tableDataParser = gson.fromJson(templateCursor.next().toString()
                        ,TableDataParser.class);
            }
        }catch (UnknownHostException e){
            e.printStackTrace();
        }
        return tableDataParser;
    }
}
