package com.enhanzer.pdf.extractor.model.extract.pdf;

import com.enhanzer.pdf.extractor.model.data.extract.pdf.ExtractStatus;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;


public class RequestProcessor {

    public ExtractStatus processRequest(HttpServletRequest request, ExtractStatus extractStatus) {

        boolean isMultipart = ServletFileUpload.isMultipartContent(request);

        if (isMultipart) {
            // Create a factory for disk-based file items
            FileItemFactory factory = new DiskFileItemFactory();
            // Create a new file upload handler
            ServletFileUpload upload = new ServletFileUpload(factory);
            try {
                // Parse the request
                List items = upload.parseRequest(request);

                // assign the form fields Template Name, Category Names to the templateProperties array
                AssignFormValues formValues = new AssignFormValues();
                formValues.assignTemplateProperties(items, extractStatus);

                DocumentIdAuthenticator authenticator = new DocumentIdAuthenticator();
                authenticator.isDocIdValid(extractStatus);

                if (extractStatus.getStatus()){

                PdfFileProcessor fileProcessor = new PdfFileProcessor();
                fileProcessor.processFile(items, extractStatus);

                DataElementsProcessor dataElementsProcessor = new DataElementsProcessor();
                    /* Check to ensure the uploaded file is a PDF. Status set in the isPDF Function*/
                    if (extractStatus.getStatus()) {

                        /* Retrieves DataElements [text,image,table] from templateInfo MongoDB Collection and
                        Insert in to extractedData Mongo DB collection
                        */
                        dataElementsProcessor.processTextDataElements(extractStatus);

                        dataElementsProcessor.processImageDataElements(extractStatus);

                        dataElementsProcessor.processTableDataElements(extractStatus);
                    }
            }

            } catch (FileUploadException e) {
                e.printStackTrace();
                extractStatus.setErrorCause("File Upload Exception Occurred");
            } catch (IOException e) {
                e.printStackTrace();
                extractStatus.setErrorCause("PDF File not Found. Contact ADMIN");
            } catch (Exception e) {
                e.printStackTrace();
                extractStatus.setErrorCause("Exception Occurred. Contact ADMIN");
            }
        }
        return extractStatus;
    }
}
