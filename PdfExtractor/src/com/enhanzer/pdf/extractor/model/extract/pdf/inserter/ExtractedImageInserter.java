package com.enhanzer.pdf.extractor.model.extract.pdf.inserter;

import com.enhanzer.pdf.extractor.model.data.extract.pdf.ExtractStatus;
import com.enhanzer.pdf.extractor.model.data.template.info.image.ImageDataElement;
import com.enhanzer.pdf.extractor.model.data.template.info.image.ImageDataParser;
import com.enhanzer.pdf.extractor.model.db.connect.MongoDB;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class ExtractedImageInserter {

    private static final String host="localhost";
    private static final String dbName="staging";
    private static final String templatesColl="extractedData";
    private static final int    port=27017;


    public void insert(ImageDataParser imageDataParser,ExtractStatus extractStatus){

        try {
            MongoDB mongoDB = new MongoDB();
            DB mongoClient = mongoDB.createMongoClient(host, dbName, port);
            DBCollection templateCollection = mongoDB.getCollection(mongoClient, templatesColl);
            BasicDBObject searchQuery = new BasicDBObject();
            /*
            Returns the DB cursor object where the mainCategory=form.mainCategory and
            subCategory=form.subCategory from the templates Collections
             */
            String documentId=extractStatus.getDocumentId();

            searchQuery.put("mainCategory", imageDataParser.getMainCategory());
            searchQuery.put("subCategory", imageDataParser.getSubCategory());
            searchQuery.put("templateName", imageDataParser.getTemplateName());
            searchQuery.put("documentId", documentId);
            searchQuery.put("dataType", imageDataParser.getDataType());


            List<ImageDataElement> imageDataElements = imageDataParser.getImageDataElements();

            for(ImageDataElement i:imageDataElements){
                DBCursor templateCursor = templateCollection.find(searchQuery);

                if (templateCursor.hasNext()) {
                    // If record exists update the record
                    updateRecord(i,templateCollection, searchQuery);
                } else { // If there is no record exists create record and input
                    createNewRecord(imageDataParser, i,templateCollection,documentId);
                }
            }
        }catch (UnknownHostException e){
            e.printStackTrace();
        }
    }

    public void createNewRecord(ImageDataParser imageDataParser, ImageDataElement imageDataElement,
                                DBCollection templateCollection,String documentId ) {

        BasicDBObject insertObject = new BasicDBObject();
        insertObject.put("mainCategory", imageDataParser.getMainCategory());
        insertObject.put("subCategory", imageDataParser.getSubCategory());
        insertObject.put("templateName", imageDataParser.getTemplateName());
        insertObject.put("documentId", documentId);
        insertObject.put("dataType", imageDataParser.getDataType());

        List<BasicDBObject> imageDataElementsInsert = new ArrayList<BasicDBObject>();

        BasicDBObject imageElementObject = new BasicDBObject();

        imageElementObject.put("metaId", imageDataElement.getMetaId());
        imageElementObject.put("extractedImage", imageDataElement.getExtractedImage());
        imageDataElementsInsert.add(imageElementObject);

        insertObject.put("imageDataElements", imageDataElementsInsert);

        templateCollection.insert(insertObject);

    }

    public void updateRecord(ImageDataElement imageDataElement,
                             DBCollection templateCollection , BasicDBObject searchQuery ){

        BasicDBObject imageElementObject = new BasicDBObject();

        imageElementObject.put("metaId", imageDataElement.getMetaId());
        imageElementObject.put("extractedImage", imageDataElement.getExtractedImage());

        BasicDBObject updateObject = new BasicDBObject();
        updateObject.put("$push", new BasicDBObject("imageDataElements", imageElementObject));
        templateCollection.update(searchQuery, updateObject);

    }
}
