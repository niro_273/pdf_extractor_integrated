package com.enhanzer.pdf.extractor.model.extract.pdf;


import com.enhanzer.pdf.extractor.model.data.extract.pdf.ExtractStatus;
import com.enhanzer.pdf.extractor.model.data.templates.Template;
import com.enhanzer.pdf.extractor.model.data.templates.TemplatesParser;
import com.enhanzer.pdf.extractor.model.data.upload.template.UploadStatus;
import com.enhanzer.pdf.extractor.model.db.connect.MongoDB;
import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

import java.net.UnknownHostException;
import java.util.List;

public class DocumentIdAuthenticator {

    /*
   extractedData Collection Data Format :
   {'mainCategory':'mainCategory',subCategory:'subCategory','templateName':'templateName'
   documentId:'documentId',dataType:'text/image/table', image/text/table~DataElements : 'dataElements'}
    */
    private static final String host="localhost";
    private static final String dbName="staging";
    private static final String dataColl="extractedData";
    private static final int    port=27017;


    public ExtractStatus isDocIdValid(ExtractStatus extractStatus) throws UnknownHostException {

        MongoDB mongoDB=new MongoDB();
        DB mongoClient = mongoDB.createMongoClient(host,dbName,port);
        DBCollection dataCollection = mongoDB.getCollection(mongoClient,dataColl);
        BasicDBObject basicDBObject = new BasicDBObject();
        /*
        Returns the DB cursor object where the mainCategory=form.mainCategory and
        subCategory=form.subCategory from the templates Collections
         */
        basicDBObject.put("mainCategory", extractStatus.getMainCategory());
        basicDBObject.put("subCategory", extractStatus.getSubCategory());
        basicDBObject.put("templateName", extractStatus.getTemplateName());
        basicDBObject.put("documentId", extractStatus.getDocumentId());
        DBCursor templateCursor = dataCollection.find(basicDBObject);

        if (templateCursor.hasNext()){
            extractStatus.setStatus(false);
            extractStatus.setErrorCause("Document ID Already Taken");
        }
        else {
            extractStatus.setStatus(true);
        }

        return extractStatus;
    }
}
