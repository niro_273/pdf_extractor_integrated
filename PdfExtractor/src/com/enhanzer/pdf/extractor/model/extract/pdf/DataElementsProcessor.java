package com.enhanzer.pdf.extractor.model.extract.pdf;


import com.enhanzer.pdf.extractor.model.data.extract.pdf.ExtractStatus;
import com.enhanzer.pdf.extractor.model.data.template.info.image.ImageDataElement;
import com.enhanzer.pdf.extractor.model.data.template.info.image.ImageDataParser;
import com.enhanzer.pdf.extractor.model.data.template.info.table.TableDataParser;
import com.enhanzer.pdf.extractor.model.data.template.info.text.TextDataElement;
import com.enhanzer.pdf.extractor.model.data.template.info.text.TextDataParser;
import com.enhanzer.pdf.extractor.model.extract.pdf.inserter.ExtractedImageInserter;
import com.enhanzer.pdf.extractor.model.extract.pdf.inserter.ExtractedTableInserter;
import com.enhanzer.pdf.extractor.model.extract.pdf.inserter.ExtractedTextInserter;
import com.enhanzer.pdf.extractor.model.extract.pdf.retriever.ImageDataRetriever;
import com.enhanzer.pdf.extractor.model.extract.pdf.retriever.TableDataRetriever;
import com.enhanzer.pdf.extractor.model.extract.pdf.retriever.TextDataRetriever;
import com.enhanzer.pdf.extractor.model.extract.pdf.table.DataProcessor;
import com.enhanzer.pdf.extractor.model.extractors.image.FullSelectionImageExtractor;
import com.enhanzer.pdf.extractor.model.extractors.text.FullSelectionTextExtractor;
import com.enhanzer.pdf.extractor.model.extractors.text.MetaSelectionTextExtractor;
import org.apache.pdfbox.exceptions.CryptographyException;
import org.apache.pdfbox.pdmodel.PDDocument;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class DataElementsProcessor {

    public void processTextDataElements(ExtractStatus extractStatus) throws IOException {
        TextDataRetriever textDataRetriever = new TextDataRetriever();
        TextDataParser textDataParser = textDataRetriever.retrieveTextData(extractStatus);

        /* Check if no text data available to extract then skip*/
        if(textDataParser!=null)
        {
        List<TextDataElement> textDataElements = textDataParser.getTextDataElements();

        PDDocument doc = PDDocument.load(extractStatus.getUploadedPdfFile());
        String extractedText;

        for (TextDataElement t : textDataElements) {
            if (t.getMetaX1() == -1) {
                FullSelectionTextExtractor fullExtractor = new FullSelectionTextExtractor();
                extractedText = fullExtractor.extract(doc, t);
            } else {
                MetaSelectionTextExtractor metaExtractor = new MetaSelectionTextExtractor();
                extractedText = metaExtractor.extractWithOutLabel(doc, t);
            }
            t.setExtractedText(extractedText);
        }

        ExtractedTextInserter textInserter = new ExtractedTextInserter();
        textInserter.insert(textDataParser, extractStatus);
    }
    }

    public void processImageDataElements(ExtractStatus extractStatus) throws IOException {
        ImageDataRetriever imageDataRetriever=new ImageDataRetriever();
        ImageDataParser imageDataParser=imageDataRetriever.retrieveImageData(extractStatus);

        /* If there is no record exists for image data extraction skip this step*/
        if(imageDataParser!=null) {
            List<ImageDataElement> imageDataElements = imageDataParser.getImageDataElements();

            PDDocument doc = PDDocument.load(extractStatus.getUploadedPdfFile());
            String imageAbsolutePath;

            String imageWrtiePath = getImageWritePath(extractStatus);

            for (ImageDataElement i : imageDataElements) {
                FullSelectionImageExtractor imageExtractor = new FullSelectionImageExtractor();
                imageAbsolutePath = imageExtractor.extractImage(imageWrtiePath, doc, i);
                i.setExtractedImage(imageAbsolutePath);
            }

            ExtractedImageInserter imageInserter = new ExtractedImageInserter();
            imageInserter.insert(imageDataParser, extractStatus);
        }
    }

    public void processTableDataElements(ExtractStatus extractStatus) throws IOException, CryptographyException {
        TableDataRetriever tableDataRetriever=new TableDataRetriever();
        TableDataParser tableDataParser=tableDataRetriever.retrieveTableData(extractStatus);

        DataProcessor processor=new DataProcessor();
        processor.processTable(tableDataParser,extractStatus);

        ExtractedTableInserter inserter=new ExtractedTableInserter();
        inserter.insert(tableDataParser,extractStatus);

    }

    public String getImageWritePath(ExtractStatus extractStatus){
        String imageWritePath;
        PdfFileProcessor pdfFileProcessor=new PdfFileProcessor();
        String[] spaceReplaced= pdfFileProcessor.replaceSpace(extractStatus);
        imageWritePath=extractStatus.getRootPath()+ "extracts"+File.separator+
                        spaceReplaced[2]+File.separator+spaceReplaced[3]+File.separator+spaceReplaced[0]+
                        File.separator+spaceReplaced[1];

        return imageWritePath;
    }
}
