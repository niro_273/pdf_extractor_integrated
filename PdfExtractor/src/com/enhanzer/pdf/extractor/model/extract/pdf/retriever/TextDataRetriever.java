package com.enhanzer.pdf.extractor.model.extract.pdf.retriever;

import com.enhanzer.pdf.extractor.model.data.extract.pdf.ExtractStatus;
import com.enhanzer.pdf.extractor.model.data.template.info.text.TextDataElement;
import com.enhanzer.pdf.extractor.model.data.template.info.text.TextDataParser;
import com.enhanzer.pdf.extractor.model.data.templates.Template;
import com.enhanzer.pdf.extractor.model.data.upload.template.UploadStatus;
import com.enhanzer.pdf.extractor.model.db.connect.MongoDB;
import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

import java.net.UnknownHostException;
import java.util.List;


public class TextDataRetriever {

    private static final String host="localhost";
    private static final String dbName="staging";
    private static final String templatesColl="templateInfo";
    private static final int    port=27017;


    public TextDataParser retrieveTextData(ExtractStatus extractStatus){
        TextDataParser textDataParser=null;
        try {

            MongoDB mongoDB=new MongoDB();
            DB mongoClient = mongoDB.createMongoClient(host,dbName,port);
            DBCollection templateCollection = mongoDB.getCollection(mongoClient,templatesColl);
            BasicDBObject basicDBObject = new BasicDBObject();
            /*
            Returns the DB cursor object where the mainCategory=form.mainCategory and
            subCategory=form.subCategory from the templates Collections
             */
            basicDBObject.put("mainCategory", extractStatus.getMainCategory());
            basicDBObject.put("subCategory", extractStatus.getSubCategory());
            basicDBObject.put("templateName", extractStatus.getTemplateName());
            basicDBObject.put("dataType", "text");
            DBCursor templateCursor = templateCollection.find(basicDBObject);

            if (templateCursor.hasNext()){
                // If there is a Record Present parse the MongoObject returned
                Gson gson = new Gson();
                textDataParser = gson.fromJson(templateCursor.next().toString()
                        ,TextDataParser.class);
            }
        }catch (UnknownHostException e){
            e.printStackTrace();
        }
        return textDataParser;
    }
}
