package com.enhanzer.pdf.extractor.model.extract.pdf;


import com.enhanzer.pdf.extractor.model.data.extract.pdf.ExtractStatus;
import com.enhanzer.pdf.extractor.model.data.upload.template.UploadStatus;
import org.apache.commons.fileupload.FileItem;

import java.util.Iterator;
import java.util.List;

public class AssignFormValues {

    /*
    This method returns the form value of mainCategory , subCategory and templateName
    as the templateProperties Array
     */
    public ExtractStatus assignTemplateProperties(List items,ExtractStatus extractStatus){

        Iterator propertyIterator = items.iterator();

        while (propertyIterator.hasNext()) {
            FileItem templateProperty = (FileItem) propertyIterator.next();
            // Only parse the request which are of form filed
            if (templateProperty.isFormField()) {
                String fieldName = templateProperty.getFieldName();

                // Assign the Value for the mainCategory
                if(fieldName.equals("mainCategory")){
                    extractStatus.setMainCategory(templateProperty.getString());
                }

                // Assign the value for the subcategory
                if(fieldName.equals("subCategory")){
                    extractStatus.setSubCategory(templateProperty.getString());
                }
                // Assign the value for the template name
                if(fieldName.equals("templateName")){
                    extractStatus.setTemplateName(templateProperty.getString());
                }
                // Assign the value for the template name
                if(fieldName.equals("documentId")){
                    extractStatus.setDocumentId(templateProperty.getString());
                }
            }
        }
        return extractStatus;
    }
}
