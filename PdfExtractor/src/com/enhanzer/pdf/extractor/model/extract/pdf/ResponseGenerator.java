package com.enhanzer.pdf.extractor.model.extract.pdf;


import com.enhanzer.pdf.extractor.model.data.extract.pdf.ExtractResponse;
import com.enhanzer.pdf.extractor.model.data.extract.pdf.ExtractStatus;

public class ResponseGenerator {
    public ExtractResponse generateResponse(ExtractStatus extractStatus){
        ExtractResponse extractResponse=new ExtractResponse();
        extractResponse.setStatus(extractStatus.getStatus());

        if(extractResponse.isStatus()) {
            extractResponse.setSuccessMsg(extractStatus.getDocumentId()+" Extracted Successfully");
        }else
            extractResponse.setErrorCause(extractStatus.getErrorCause());
        return extractResponse;

    }
}
