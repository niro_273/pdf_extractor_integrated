package com.enhanzer.pdf.extractor.model.extract.pdf.inserter;


import com.enhanzer.pdf.extractor.model.data.extract.pdf.ExtractStatus;
import com.enhanzer.pdf.extractor.model.data.template.info.text.TextDataElement;
import com.enhanzer.pdf.extractor.model.data.template.info.text.TextDataParser;
import com.enhanzer.pdf.extractor.model.db.connect.MongoDB;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class ExtractedTextInserter {

    private static final String host="localhost";
    private static final String dbName="staging";
    private static final String templatesColl="extractedData";
    private static final int    port=27017;


    public void insert(TextDataParser textDataParser,ExtractStatus extractStatus){

        try {
            MongoDB mongoDB = new MongoDB();
            DB mongoClient = mongoDB.createMongoClient(host, dbName, port);
            DBCollection templateCollection = mongoDB.getCollection(mongoClient, templatesColl);
            BasicDBObject searchQuery = new BasicDBObject();
            /*
            Returns the DB cursor object where the mainCategory=form.mainCategory and
            subCategory=form.subCategory from the templates Collections
             */
            String documentId=extractStatus.getDocumentId();

            searchQuery.put("mainCategory", textDataParser.getMainCategory());
            searchQuery.put("subCategory", textDataParser.getSubCategory());
            searchQuery.put("templateName", textDataParser.getTemplateName());
            searchQuery.put("documentId", documentId);
            searchQuery.put("dataType", textDataParser.getDataType());


            List<TextDataElement> textDataElements = textDataParser.getTextDataElements();

            for(TextDataElement t:textDataElements){
                DBCursor templateCursor = templateCollection.find(searchQuery);

                if (templateCursor.hasNext()) {
                    // If record exists update the record
                    updateRecord(t,templateCollection, searchQuery);
                } else { // If there is no record exists create record and input
                    createNewRecord(textDataParser, t,templateCollection,documentId);
                }
            }
        }catch (UnknownHostException e){
            e.printStackTrace();
        }
    }

    public void createNewRecord(TextDataParser textDataParser, TextDataElement textDataElement,
                                DBCollection templateCollection,String documentId ) {

        BasicDBObject insertObject = new BasicDBObject();
        insertObject.put("mainCategory", textDataParser.getMainCategory());
        insertObject.put("subCategory", textDataParser.getSubCategory());
        insertObject.put("templateName", textDataParser.getTemplateName());
        insertObject.put("documentId", documentId);
        insertObject.put("dataType", textDataParser.getDataType());

        List<BasicDBObject> textDataElementsInsert = new ArrayList<BasicDBObject>();

        BasicDBObject textElementObject = new BasicDBObject();

        textElementObject.put("metaId", textDataElement.getMetaId());
        textElementObject.put("extractedText", textDataElement.getExtractedText());
        textDataElementsInsert.add(textElementObject);

        insertObject.put("textDataElements", textDataElementsInsert);

        templateCollection.insert(insertObject);

    }

    public void updateRecord(TextDataElement textDataElement,
                             DBCollection templateCollection , BasicDBObject searchQuery ){

        BasicDBObject textElementObject = new BasicDBObject();

        textElementObject.put("metaId", textDataElement.getMetaId());
        textElementObject.put("extractedText", textDataElement.getExtractedText());

        BasicDBObject updateObject = new BasicDBObject();
        updateObject.put("$push", new BasicDBObject("textDataElements", textElementObject));
        templateCollection.update(searchQuery, updateObject);

    }
}
