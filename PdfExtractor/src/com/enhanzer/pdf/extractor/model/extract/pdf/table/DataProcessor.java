package com.enhanzer.pdf.extractor.model.extract.pdf.table;


import com.enhanzer.pdf.extractor.model.data.doc.text.positions.TextData;
import com.enhanzer.pdf.extractor.model.data.extract.pdf.ExtractStatus;
import com.enhanzer.pdf.extractor.model.data.template.info.table.Cell;
import com.enhanzer.pdf.extractor.model.data.template.info.table.Column;
import com.enhanzer.pdf.extractor.model.data.template.info.table.TableDataElement;
import com.enhanzer.pdf.extractor.model.data.template.info.table.TableDataParser;
import com.enhanzer.pdf.extractor.model.top.secret.*;
import org.apache.pdfbox.exceptions.CryptographyException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DataProcessor {

    public void processTable(TableDataParser tableDataParser,ExtractStatus extractStatus) throws IOException, CryptographyException {

        List<TableDataElement> tableDataElements;
        tableDataElements=tableDataParser.getTableDataElements();

        Cell cell;
        List<Cell> cells;
        Boolean found = true;

        for(TableDataElement ta:tableDataElements) {

            List<Column> columns;
            columns = ta.getColumns();
            TextLocationRetriever retreiver=new TextLocationRetriever();
            List<TextData> textDatas = retreiver.getTableData(extractStatus,ta.getPageNumber() - 1);

            StringBuilder testSb = new StringBuilder();

            cell=new Cell();
            cells=new ArrayList<Cell>();

            // run for each loop for the columns
            for (Column c : columns) {

                //Where the X extraction should take place
                Double startX = c.getMetaX1();
                //Where the Y extraction should take place
                Double startY = c.getMetaY1() + c.getMetaHeight();
                //Where the X extraction should end
                Double endX = c.getMetaX1() + c.getMetaWidth();
                //Where the Y extraction should end
                Double endY = ta.getTotalY1() + ta.getTotalHeight();

                // For each loop for all the textdata retreived from processing the PDF
                for (TextData te : textDatas) {

                    if (startX < te.getxDirAdj() && startY < te.getyDirAdj() && te.getyDirAdj() < endY) {

                        if (endX > te.getxDirAdj()) {

                            if (!found) {
                                cell = new Cell();
                                cell.setValue(testSb);
                                cells.add(cell);

                                testSb = new StringBuilder();
                                found = true;
                            }
                            testSb.append(te.getCharacter());
                        } else {
                            found = false;
                        }
                    }
                }

                if (!found) {
                    cell = new Cell();
                    cell.setValue(testSb);
                    cells.add(cell);

                    testSb = new StringBuilder();
                    found = true;
                }
                if (cell.getValue().length() != 0) {
                    c.setCellList(cells);
                    cells = new ArrayList<Cell>();
                }


            }
        }
        ExtractedTableInserter inserter=new ExtractedTableInserter();
        inserter.insert(tableDataParser,extractStatus);

    }
}


