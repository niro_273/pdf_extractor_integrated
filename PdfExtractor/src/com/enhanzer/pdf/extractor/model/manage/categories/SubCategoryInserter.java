package com.enhanzer.pdf.extractor.model.manage.categories;


import com.enhanzer.pdf.extractor.model.data.manage.categories.ManageCategoriesData;
import com.enhanzer.pdf.extractor.model.data.template.info.text.TextDataParser;
import com.enhanzer.pdf.extractor.model.data.templates.TemplatesParser;
import com.enhanzer.pdf.extractor.model.db.connect.MongoDB;
import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

import java.net.UnknownHostException;

public class SubCategoryInserter {

    /*
   Templates Collection Data Format :
   {'mainCategory':'mainCategory',subCategory:'subCategory','templateName':'templateName'}
    */
    private static final String host="localhost";
    private static final String dbName="staging";
    private static final String templatesColl="templates";
    private static final int    port=27017;


    public ManageCategoriesData processCreateRequest(ManageCategoriesData manageCategoriesData){
        try {

            manageCategoriesData.setStatus(true);
            MongoDB mongoDB=new MongoDB();
            DB mongoClient = mongoDB.createMongoClient(host, dbName, port);
            DBCollection templatesCollection = mongoDB.getCollection(mongoClient,templatesColl);

            BasicDBObject basicDBObject=new BasicDBObject();
            basicDBObject.put("mainCategory", manageCategoriesData.getMainCategory());
            basicDBObject.put("subCategory", manageCategoriesData.getSubCategory());
            DBCursor templatesCursor = templatesCollection.find(basicDBObject);

            if (templatesCursor.hasNext()) {
                manageCategoriesData.setStatus(false);
                manageCategoriesData.setStatusMessage(manageCategoriesData.getSubCategory()+ " already exists");
            }

            if(manageCategoriesData.getStatus()){
                createSC(manageCategoriesData, templatesCollection);
            }

        }catch (UnknownHostException e){
            e.printStackTrace();
        }
        return manageCategoriesData;
    }

    public ManageCategoriesData createSC(ManageCategoriesData manageCategoriesData,DBCollection templatesCollection){

        BasicDBObject searchQuery=new BasicDBObject();
        searchQuery.put("mainCategory", manageCategoriesData.getMainCategory());
        DBCursor templatesCursor = templatesCollection.find(searchQuery);

        if (templatesCursor.size()==1) {
            Gson gson=new Gson();
            TemplatesParser templatesParser=gson.fromJson(templatesCursor.next().toString(), TemplatesParser.class);
                if(templatesParser.getSubCategory()==null){
                    BasicDBObject updateDoc = new BasicDBObject();
                    updateDoc.append("$set", new BasicDBObject().append("subCategory", manageCategoriesData.getSubCategory()));
                    templatesCollection.update(searchQuery, updateDoc);

                }else {

                    BasicDBObject insertObject=new BasicDBObject();
                    insertObject.put("mainCategory", manageCategoriesData.getMainCategory());
                    insertObject.put("subCategory", manageCategoriesData.getSubCategory());
                    templatesCollection.insert(insertObject);

                }
        }else {
            BasicDBObject insertObject=new BasicDBObject();
            insertObject.put("mainCategory", manageCategoriesData.getMainCategory());
            insertObject.put("subCategory", manageCategoriesData.getSubCategory());
            templatesCollection.insert(insertObject);
        }

        manageCategoriesData.setStatusMessage(manageCategoriesData.getSubCategory()+ " successfully created");

        return manageCategoriesData;
    }
}
