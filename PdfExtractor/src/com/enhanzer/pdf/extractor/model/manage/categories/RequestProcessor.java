package com.enhanzer.pdf.extractor.model.manage.categories;


import com.enhanzer.pdf.extractor.model.data.manage.categories.ManageCategoriesData;
import com.enhanzer.pdf.extractor.model.manage.categories.delete.MainCategoryRemover;
import com.enhanzer.pdf.extractor.model.manage.categories.delete.SubCategoryRemover;
import com.enhanzer.pdf.extractor.model.manage.categories.delete.TemplateRemover;

import java.net.UnknownHostException;

public class RequestProcessor {

    public ManageCategoriesData processRequest(ManageCategoriesData manageCategoriesData){

        try{

            if(manageCategoriesData.getRequest().equals("createMainCategory")) {
                MainCategoryInserter mainCategoryInserter =new MainCategoryInserter();
                mainCategoryInserter.processCreateRequest(manageCategoriesData);
            }

            if(manageCategoriesData.getRequest().equals("createSubCategory")) {
                SubCategoryInserter subCategoryInserter =new SubCategoryInserter();
                subCategoryInserter.processCreateRequest(manageCategoriesData);
            }

            if(manageCategoriesData.getRequest().equals("deleteTemp")){
                TemplateRemover remover=new TemplateRemover();
                remover.removeTemplates(manageCategoriesData);
            }

            if(manageCategoriesData.getRequest().equals("deleteSubCat")){
                SubCategoryRemover remover=new SubCategoryRemover();
                remover.removeSubCat(manageCategoriesData);
            }

            if(manageCategoriesData.getRequest().equals("deleteMainCat")){
                MainCategoryRemover remover=new MainCategoryRemover();
                remover.removeMainCat(manageCategoriesData);
            }

        } catch (UnknownHostException e) {
            // Exception of MongoDB Not Found
            e.printStackTrace();
        }

        return manageCategoriesData;
    }
}
