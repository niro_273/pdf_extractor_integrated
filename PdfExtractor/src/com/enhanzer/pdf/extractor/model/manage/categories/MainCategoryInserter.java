package com.enhanzer.pdf.extractor.model.manage.categories;

import com.enhanzer.pdf.extractor.model.data.manage.categories.ManageCategoriesData;
import com.enhanzer.pdf.extractor.model.db.connect.MongoDB;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

import java.net.UnknownHostException;

public class MainCategoryInserter {

    /*
    Templates Collection Data Format :
    {'mainCategory':'mainCategory',subCategory:'subCategory',templates:[templateName:'name1']}
     */
    private static final String host="localhost";
    private static final String dbName="staging";
    private static final String templatesColl="templates";
    private static final int    port=27017;

    /*
    Creates a Main Category @returns ManageCategoriesData
     */
    public ManageCategoriesData processCreateRequest(ManageCategoriesData manageCategoriesData){

        try {

            manageCategoriesData.setStatus(true);
            MongoDB mongoDB=new MongoDB();
            DB mongoClient = mongoDB.createMongoClient(host, dbName, port);
            DBCollection templatesCollection = mongoDB.getCollection(mongoClient,templatesColl);

            BasicDBObject basicDBObject=new BasicDBObject();
            basicDBObject.put("mainCategory", manageCategoriesData.getMainCategory());
            DBCursor templatesCursor = templatesCollection.find(basicDBObject);

            if (templatesCursor.hasNext()) {
                manageCategoriesData.setStatus(false);
                manageCategoriesData.setStatusMessage(manageCategoriesData.getMainCategory()+ " already exists");
            }

            if(manageCategoriesData.getStatus()){
                createMC(manageCategoriesData,templatesCollection);
            }

        }catch (UnknownHostException e){
            e.printStackTrace();
        }
        return manageCategoriesData;
    }

    public ManageCategoriesData createMC(ManageCategoriesData manageCategoriesData,DBCollection templatesCollection){

        BasicDBObject insertObject=new BasicDBObject();
        insertObject.put("mainCategory", manageCategoriesData.getMainCategory());
        templatesCollection.insert(insertObject);
        manageCategoriesData.setStatusMessage(manageCategoriesData.getMainCategory()+ " successfully created");

        return manageCategoriesData;
    }
}
