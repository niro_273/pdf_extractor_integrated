package com.enhanzer.pdf.extractor.model.manage.categories.delete;


import com.enhanzer.pdf.extractor.model.data.manage.categories.ManageCategoriesData;
import com.enhanzer.pdf.extractor.model.data.templates.Template;
import com.enhanzer.pdf.extractor.model.data.templates.TemplatesParser;
import com.enhanzer.pdf.extractor.model.db.connect.MongoDB;
import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

import java.net.UnknownHostException;
import java.util.List;

public class MainCategoryRemover {

    /*
   Templates Collection Data Format :
   {mainCategory:'mainCategory',subCategory:'subCategory',templates:[templateName:'name1']}
   */
    private static final String host="localhost";
    private static final String dbName="staging";
    private static final String templatesColl="templates";
    private static final String templateInfoColl="templateInfo";
    private static final int    port=27017;

  public ManageCategoriesData  removeMainCat(ManageCategoriesData data) throws UnknownHostException {


        MongoDB mongoDB=new MongoDB();
        DB mongoClient = mongoDB.createMongoClient(host,dbName,port);
        DBCollection tempColl = mongoDB.getCollection(mongoClient,templatesColl);
        DBCollection infoColl = mongoDB.getCollection(mongoClient,templateInfoColl);
        BasicDBObject searchQuery = new BasicDBObject();

        searchQuery.put("mainCategory", data.getMainCategory());

        DBCursor templatesCursor = tempColl.find(searchQuery);

        TemplatesParser templatesParser;

        while(templatesCursor.hasNext()) {
            // If there is a Record Present parse the MongoObject returned
            Gson gson = new Gson();
            templatesParser = gson.fromJson(templatesCursor.next().toString()
                    , TemplatesParser.class);
            List<Template> templates = templatesParser.getTemplates();

            searchQuery.put("subCategory",templatesParser.getSubCategory());
            for(Template t:templates){
                searchQuery.put("templateName", t.getTemplateName());
                infoColl.remove(searchQuery);
            }
            searchQuery.removeField("templateName");
            tempColl.remove(searchQuery);
        }

        data.setStatus(true);
        data.setStatusMessage(data.getMainCategory() + " Successfully Removed");


        return data;
    }

}
