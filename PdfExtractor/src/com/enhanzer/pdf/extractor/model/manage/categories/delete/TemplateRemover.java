package com.enhanzer.pdf.extractor.model.manage.categories.delete;


import com.enhanzer.pdf.extractor.model.data.manage.categories.ManageCategoriesData;
import com.enhanzer.pdf.extractor.model.db.connect.MongoDB;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;

import java.net.UnknownHostException;

public class TemplateRemover {

    /*
    Templates Collection Data Format :
    {mainCategory:'mainCategory',subCategory:'subCategory',templates:[templateName:'name1']}
    */
    private static final String host="localhost";
    private static final String dbName="staging";
    private static final String templatesColl="templates";
    private static final String templateInfoColl="templateInfo";
    private static final int    port=27017;



    public ManageCategoriesData  removeTemplates(ManageCategoriesData data) throws UnknownHostException {


        MongoDB mongoDB=new MongoDB();
        DB mongoClient = mongoDB.createMongoClient(host,dbName,port);
        DBCollection tempColl = mongoDB.getCollection(mongoClient,templatesColl);
        DBCollection infoColl = mongoDB.getCollection(mongoClient,templateInfoColl);
        BasicDBObject serachQuery = new BasicDBObject();
        /*
        Returns the DB cursor object where the mainCategory=form.mainCategory ,
        subCategory=form.subCategory
         */
        serachQuery.put("mainCategory", data.getMainCategory());
        serachQuery.put("subCategory", data.getSubCategory());

        BasicDBObject removeObject=new BasicDBObject();
        removeObject.put("templates",new BasicDBObject("templateName", data.getTemplateName()));

        tempColl.update(serachQuery, new BasicDBObject("$pull",removeObject));

        serachQuery.put("templateName" , data.getTemplateName());
        infoColl.remove(serachQuery);

        data.setStatus(true);
        data.setStatusMessage(data.getTemplateName() + " successfully removed" );

        return data;
    }
}
