package com.enhanzer.pdf.extractor.model.db.connect;

import com.mongodb.DB;
import com.mongodb.DBCollection;

import java.net.UnknownHostException;


public class MongoDB {

    public DB createMongoClient(String host,String dbName,int port) throws UnknownHostException {

        com.mongodb.MongoClient mongoClient = new com.mongodb.MongoClient(host, port);
        return mongoClient.getDB(dbName);

    }

    public DBCollection getCollection(DB mongoClient,String collectionName){
        return mongoClient.getCollection(collectionName);
    }
}
