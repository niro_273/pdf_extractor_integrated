package com.enhanzer.pdf.extractor.model.template.upload;

import com.enhanzer.pdf.extractor.model.data.upload.template.UploadStatus;
import com.enhanzer.pdf.extractor.model.db.connect.MongoDB;
import com.mongodb.*;

import java.net.UnknownHostException;


public class TemplateNameInsert {
    /*
    Templates Collection Data Format :
    {'mainCategory':'mainCategory',subCategory:'subCategory','templates':['templateName':'templateName']}
     */
    private static final String host="localhost";
    private static final String dbName="staging";
    private static final String templatesColl="templates";
    private static final int    port=27017;

    public  UploadStatus insertTemplateName(UploadStatus uploadStatus){
        try {

            MongoDB mongoDB=new MongoDB();
            DB mongoClient = mongoDB.createMongoClient(host, dbName, port);
            DBCollection templatesCollection = mongoDB.getCollection(mongoClient, templatesColl);

            BasicDBObject searchQuery = new BasicDBObject();
            searchQuery.put("mainCategory", uploadStatus.getMainCategory());
            searchQuery.put("subCategory", uploadStatus.getSubCategory());

            BasicDBObject templateObject = new BasicDBObject();
            templateObject.put("templateName", uploadStatus.getTemplateName());
            templateObject.put("pdfFile",uploadStatus.getUploadedPdfFile());

            BasicDBObject updateObject=new BasicDBObject();
            updateObject.put("$push",new BasicDBObject("templates", templateObject));
            templatesCollection.update(searchQuery, updateObject);
        }catch (UnknownHostException e){
            e.printStackTrace();
        }
    return uploadStatus;

    }
}
