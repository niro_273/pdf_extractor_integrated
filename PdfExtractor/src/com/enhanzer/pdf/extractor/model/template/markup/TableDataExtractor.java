package com.enhanzer.pdf.extractor.model.template.markup;

import com.enhanzer.pdf.extractor.model.data.markup.template.MarkUpResponse;
import com.enhanzer.pdf.extractor.model.data.template.info.table.Column;
import com.enhanzer.pdf.extractor.model.data.template.info.table.TableDataElement;
import com.enhanzer.pdf.extractor.model.data.template.info.table.TableDataParser;
import com.enhanzer.pdf.extractor.model.extractors.tabel.TableExtractor;
import com.enhanzer.pdf.extractor.model.template.markup.calculate.coordinates.TableDataCoordinates;
import com.enhanzer.pdf.extractor.model.template.markup.pdf.retreiver.TablePDDocument;
import com.google.gson.Gson;
import org.apache.pdfbox.pdmodel.PDDocument;

import java.io.IOException;
import java.util.List;


public class TableDataExtractor {

    public MarkUpResponse extractTable(String jsonRequest){
        String extractedTable=null;

        MarkUpResponse markUpResponse=new MarkUpResponse();

        /* Assign the input from the user to the tableDataParser */
        Gson gson=new Gson();
        TableDataParser tableDataParser;
        tableDataParser=gson.fromJson(jsonRequest, TableDataParser.class);

        List<TableDataElement> tableDataElements=tableDataParser.getTableDataElements();
        try {
            TablePDDocument tablePDDocument=new TablePDDocument();
            PDDocument doc=tablePDDocument.retrievePDDoc(tableDataParser);

            /* Get the first textDataElement because only  textDataElement to extract*/
            TableDataElement tableDataElement=tableDataElements.get(0);

            TableDataCoordinates tableDataCoordinates=new TableDataCoordinates();
            /* Set Values for the PDF width, Height and Rotation for the first tableDataElement*/
            tableDataCoordinates.setPdfProperties(doc, tableDataParser);
            /* Recalculate and set coordinates according to the actual pdf width and height & Page Rotation */
            tableDataCoordinates.calculateCoordinates(tableDataParser);

            TableExtractor tableExtractor=new TableExtractor();
            tableExtractor.extract(doc,tableDataElement);

            List<Column> columns=tableDataElement.getColumns();

            // TO DO Remove these lines after implementing a method to set the extracted table text properly
            for(Column c:columns) {
                System.out.println(c.getExtractedValues());
            }

            doc.close();
        } catch (IOException e) {
             //Exception for file not found and Image not Found
            e.printStackTrace();
        }
        markUpResponse.setDataType(tableDataParser.getDataType());
        // TO DO Implement a method to set the extracted table text properly
        markUpResponse.setExtractedData(extractedTable);

        return markUpResponse;
    }
}
