package com.enhanzer.pdf.extractor.model.template.markup.pdf.retreiver;


import com.enhanzer.pdf.extractor.model.data.template.info.table.TableDataParser;
import com.enhanzer.pdf.extractor.model.data.template.info.text.TextDataParser;
import com.enhanzer.pdf.extractor.model.data.templates.Template;
import com.enhanzer.pdf.extractor.model.data.templates.TemplatesParser;
import com.enhanzer.pdf.extractor.model.db.connect.MongoDB;
import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import org.apache.pdfbox.pdmodel.PDDocument;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

public class TablePDDocument {

    private static final String host="localhost";
    private static final String dbName="staging";
    private static final String templatesColl="templates";
    private static final int    port=27017;

    public PDDocument retrievePDDoc (TableDataParser tableDataParser) throws IOException {
        tableDataParser=retrieveUploadedPDf(tableDataParser);
        String pdfFile=tableDataParser.getPdfFile();
        PDDocument doc =PDDocument.load(pdfFile);
        return doc;
    }

    public TableDataParser retrieveUploadedPDf(TableDataParser tableDataParser){

        try {
            MongoDB mongoDB=new MongoDB();
            DB mongoClient = mongoDB.createMongoClient(host,dbName,port);
            DBCollection templateCollection = mongoDB.getCollection(mongoClient,templatesColl);
            BasicDBObject basicDBObject = new BasicDBObject();
            /*
            Returns the DB cursor object where the mainCategory=form.mainCategory and
            subCategory=form.subCategory from the templates Collections
             */
            basicDBObject.put("mainCategory", tableDataParser.getMainCategory());
            basicDBObject.put("subCategory", tableDataParser.getSubCategory());
            DBCursor templateCursor = templateCollection.find(basicDBObject);

            if (templateCursor.hasNext()){
                // If there is a Record Present parse the MongoObject and Return the pdf File Location
                Gson gson = new Gson();
                TemplatesParser templateInfo = gson.fromJson(templateCursor.next().toString()
                        ,TemplatesParser.class);
                List<Template> templates=templateInfo.getTemplates();

                for(Template t:templates){
                    if(t.getTemplateName().equals(tableDataParser.getTemplateName())){
                        tableDataParser.setPdfFile(t.getPdfFile());
                    }
                }
            }
        }catch (UnknownHostException e){
            e.printStackTrace();
        }
        return  tableDataParser;
    }
}

