package com.enhanzer.pdf.extractor.model.template.markup;

import com.enhanzer.pdf.extractor.model.data.markup.template.MarkUpRequest;
import com.enhanzer.pdf.extractor.model.data.markup.template.MarkUpResponse;
import com.google.gson.Gson;

import java.io.IOException;

public class MarkUpRequestProcessor {
    public MarkUpResponse processRequest(String jsonRequest) throws IOException {
        MarkUpResponse markUpResponse=new MarkUpResponse();

        Gson gson=new Gson();
        MarkUpRequest markUpRequest=gson.fromJson(jsonRequest,MarkUpRequest.class);
        String dataType=markUpRequest.getDataType();
        String status=markUpRequest.getStatus();


        if(status.equals("extract")){

            if(dataType.equals("text") ){
                TextDataExtractor textDataExtractor = new TextDataExtractor();
                markUpResponse=textDataExtractor.extractText(jsonRequest);
            }

            if(dataType.equals("image") ){
                ImageDataExtractor imageDataExtractor = new ImageDataExtractor();
                markUpResponse=imageDataExtractor.extractImage(jsonRequest);

            }

            if(dataType.equals("table") ){
                TableDataExtractor tableDataExtractor = new TableDataExtractor();
                markUpResponse=tableDataExtractor.extractTable(jsonRequest);
            }

        }

        if(status.equals("insert")){
            InsertRequestProcessor requestProcessor=new InsertRequestProcessor();
            requestProcessor.processRequest(jsonRequest);
        }

        return markUpResponse;
    }
}
