package com.enhanzer.pdf.extractor.model.template.markup.insert.coordinate;


import com.enhanzer.pdf.extractor.model.data.template.info.table.Column;
import com.enhanzer.pdf.extractor.model.data.template.info.table.TableDataElement;
import com.enhanzer.pdf.extractor.model.data.template.info.table.TableDataParser;
import com.enhanzer.pdf.extractor.model.db.connect.MongoDB;
import com.enhanzer.pdf.extractor.model.template.markup.calculate.coordinates.TableDataCoordinates;
import com.enhanzer.pdf.extractor.model.template.markup.pdf.retreiver.TablePDDocument;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import org.apache.pdfbox.pdmodel.PDDocument;


import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class TableDataInserter {

    private static final String host="localhost";
    private static final String dbName="staging";
    private static final String templatesColl="templateInfo";
    private static final int    port=27017;

    public void insert(TableDataParser tableDataParser){

        try {
            MongoDB mongoDB=new MongoDB();
            DB mongoClient = mongoDB.createMongoClient(host,dbName,port);
            DBCollection templateCollection = mongoDB.getCollection(mongoClient,templatesColl);
            BasicDBObject searchQuery = new BasicDBObject();

            searchQuery.put("mainCategory", tableDataParser.getMainCategory());
            searchQuery.put("subCategory", tableDataParser.getSubCategory());
            searchQuery.put("templateName", tableDataParser.getTemplateName());
            searchQuery.put("dataType", tableDataParser.getDataType());


            List<TableDataElement> tableDataElements = tableDataParser.getTableDataElements();

            for(TableDataElement ta:tableDataElements) {
                DBCursor templateCursor = templateCollection.find(searchQuery);
                if (templateCursor.hasNext()) {
                    // If record exists update the record
                    updateRecord(tableDataParser, ta,templateCollection, searchQuery);
                } else {
                    // If there is no record exists create record and input
                    createNewRecord(tableDataParser, ta,templateCollection);
                }
            }
        }catch (UnknownHostException e){
            e.printStackTrace();
        }
    }

    public void createNewRecord(TableDataParser tableDataParser,TableDataElement tableDataElement,DBCollection templateCollection ){

        BasicDBObject insertObject=new BasicDBObject();
        insertObject.put("mainCategory",tableDataParser.getMainCategory());
        insertObject.put("subCategory",tableDataParser.getSubCategory());
        insertObject.put("templateName",tableDataParser.getTemplateName());
        insertObject.put("dataType",tableDataParser.getDataType());

        List<BasicDBObject> tableDataElementsInsert=new ArrayList<BasicDBObject>();

        BasicDBObject tableElementObject=new BasicDBObject();

        tableElementObject.put("metaId",tableDataElement.getMetaId());
        tableElementObject.put("pageNumber",tableDataElement.getPageNumber());
        tableElementObject.put("pageRotation",tableDataElement.getPageRotation());

        tableElementObject.put("totalX1",tableDataElement.getTotalX1());
        tableElementObject.put("totalY1",tableDataElement.getTotalY1());
        tableElementObject.put("totalWidth",tableDataElement.getTotalWidth());
        tableElementObject.put("totalHeight",tableDataElement.getTotalHeight());

        List<Column> columns=tableDataElement.getColumns();
        ArrayList columnData = new ArrayList();

        for(Column c:columns){
            columnData.add(new BasicDBObject("metaId",c.getMetaId()).append("metaX1",c.getMetaX1())
                            .append("metaY1",c.getMetaY1()).append("metaWidth",c.getMetaWidth())
                            .append("metaHeight",c.getMetaHeight()));
        }

        tableElementObject.put("columns",columnData);
        tableDataElementsInsert.add(tableElementObject);
        insertObject.put("tableDataElements",tableDataElementsInsert);
        templateCollection.insert(insertObject);
    }

    public void updateRecord(TableDataParser tableDataParser,TableDataElement tableDataElement,DBCollection templateCollection , BasicDBObject searchQuery ){

        BasicDBObject tableElementObject = new BasicDBObject();

        tableElementObject.put("metaId",tableDataElement.getMetaId());
        tableElementObject.put("pageNumber",tableDataElement.getPageNumber());
        tableElementObject.put("pageRotation",tableDataElement.getPageRotation());

        tableElementObject.put("totalX1",tableDataElement.getTotalX1());
        tableElementObject.put("totalY1",tableDataElement.getTotalY1());
        tableElementObject.put("totalWidth",tableDataElement.getTotalWidth());
        tableElementObject.put("totalHeight",tableDataElement.getTotalHeight());

        List<Column> columns=tableDataElement.getColumns();
        ArrayList columnData = new ArrayList();

        for(Column c:columns){
            columnData.add(new BasicDBObject("metaId",c.getMetaId()).append("metaX1",c.getMetaX1())
                    .append("metaY1",c.getMetaY1()).append("metaWidth",c.getMetaWidth())
                    .append("metaHeight",c.getMetaHeight()));
        }
        tableElementObject.put("columns",columnData);

        BasicDBObject updateObject=new BasicDBObject();
        updateObject.put("$push",new BasicDBObject("tableDataElements", tableElementObject));
        templateCollection.update(searchQuery, updateObject);

    }
}
