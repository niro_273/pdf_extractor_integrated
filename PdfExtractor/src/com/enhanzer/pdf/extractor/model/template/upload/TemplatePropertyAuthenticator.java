package com.enhanzer.pdf.extractor.model.template.upload;


import com.enhanzer.pdf.extractor.model.data.templates.Template;
import com.enhanzer.pdf.extractor.model.data.templates.TemplatesParser;
import com.enhanzer.pdf.extractor.model.data.upload.template.UploadStatus;
import com.enhanzer.pdf.extractor.model.db.connect.MongoDB;
import com.google.gson.Gson;
import com.mongodb.*;

import java.net.UnknownHostException;
import java.util.List;

public class TemplatePropertyAuthenticator {

    /*
    Templates Collection Data Format :
    {'mainCategory':'mainCategory',subCategory:'subCategory','templateName':'templateName'}
     */
    private static final String host="localhost";
    private static final String dbName="staging";
    private static final String templatesColl="templates";
    private static final int    port=27017;


    public  UploadStatus isNameValid(UploadStatus uploadStatus){

        try {

            MongoDB mongoDB=new MongoDB();
            DB mongoClient = mongoDB.createMongoClient(host,dbName,port);
            DBCollection templateCollection = mongoDB.getCollection(mongoClient,templatesColl);
            BasicDBObject basicDBObject = new BasicDBObject();
            /*
            Returns the DB cursor object where the mainCategory=form.mainCategory and
            subCategory=form.subCategory from the templates Collections
             */
            basicDBObject.put("mainCategory", uploadStatus.getMainCategory());
            basicDBObject.put("subCategory", uploadStatus.getSubCategory());
            DBCursor templateCursor = templateCollection.find(basicDBObject);

            if (templateCursor.hasNext()){
                // If there is a Record Present parse the MongoObject returned
                Gson gson = new Gson();
                TemplatesParser templateInfo = gson.fromJson(templateCursor.next().toString()
                        ,TemplatesParser.class);
                List<Template> templates=templateInfo.getTemplates();

                for(Template t:templates){
                    if(t.getTemplateName().equals(uploadStatus.getTemplateName())){
                        uploadStatus.setIsTemplateNameValid(false);
                        uploadStatus.setTemplateNameErrorCause("Template Name Already Taken");
                        return uploadStatus;
                    }
                }
            }
        }catch (UnknownHostException e){
            e.printStackTrace();
        }
        uploadStatus.setIsTemplateNameValid(true);
        return uploadStatus;
    }
}
