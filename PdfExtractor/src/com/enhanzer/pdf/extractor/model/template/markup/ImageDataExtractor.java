package com.enhanzer.pdf.extractor.model.template.markup;

import com.enhanzer.pdf.extractor.model.data.markup.template.MarkUpResponse;
import com.enhanzer.pdf.extractor.model.data.template.info.image.ImageDataElement;
import com.enhanzer.pdf.extractor.model.data.template.info.image.ImageDataParser;
import com.enhanzer.pdf.extractor.model.extractors.image.FullSelectionImageExtractor;
import com.enhanzer.pdf.extractor.model.template.markup.calculate.coordinates.ImageDataCoordinates;
import com.enhanzer.pdf.extractor.model.template.markup.pdf.retreiver.ImagePDDocument;
import com.google.gson.Gson;
import org.apache.pdfbox.pdmodel.PDDocument;

import java.io.File;
import java.io.IOException;
import java.util.List;


public class ImageDataExtractor {

    public MarkUpResponse extractImage(String jsonRequest) throws IOException {
        String imageFile;
        String imageRelativePath;
        String imageWritePath[];

        MarkUpResponse markUpResponse=new MarkUpResponse();

        Gson gson=new Gson();
        ImageDataParser imageDataParser;
        imageDataParser=gson.fromJson(jsonRequest, ImageDataParser.class);

        List<ImageDataElement> imageDataElements=imageDataParser.getImageDataElements();

        ImagePDDocument imagePDDocument=new ImagePDDocument();
        PDDocument doc=imagePDDocument.retrievePDDoc(imageDataParser);

        /* Get the first textDataElement because only  textDataElement to extract*/
        ImageDataElement imageDataElement=imageDataElements.get(0);

        ImageDataCoordinates imageDataCoordinates=new ImageDataCoordinates();
        /* Set Values for the PDF width, Height and Rotation for the first imageDataElement*/
        imageDataCoordinates.setPdfProperties(doc, imageDataParser);
        /* Recalculate and set coordinates according to the actual pdf width and height & Page Rotation */
        imageDataCoordinates.calculateCoordinates(imageDataParser);

        imageWritePath=imageDataCoordinates.getPdfWritePath(imageDataParser);
        /*
            set the image write path to
            RootPath/uploads/mainCategory/subCategory/templateName/images/extracts
        */
        imageWritePath[0]=imageWritePath[0]+File.separator + "images" + File.separator + "extracts";

        FullSelectionImageExtractor imageExtractor=new FullSelectionImageExtractor();
        imageFile=imageExtractor.extractImage(imageWritePath[0],doc,imageDataElement);

        imageRelativePath=getImageRelativePath(imageFile);

        doc.close();

        markUpResponse.setDataType(imageDataParser.getDataType());
        markUpResponse.setExtractedData(imageRelativePath);
        return markUpResponse;
    }

    /*
    Method will split the String when the 'uploads' << word begins
    IMPORTANT  - When deploying the system do not deploy to a folder named uploads
     */
    public String getImageRelativePath(String imageFile){
        String[] splits = imageFile.split("uploads",2);
        return "uploads"+splits[1];
    }
}
