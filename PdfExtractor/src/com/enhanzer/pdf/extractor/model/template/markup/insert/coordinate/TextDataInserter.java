package com.enhanzer.pdf.extractor.model.template.markup.insert.coordinate;

import com.enhanzer.pdf.extractor.model.data.template.info.text.TextDataElement;
import com.enhanzer.pdf.extractor.model.data.template.info.text.TextDataParser;
import com.enhanzer.pdf.extractor.model.db.connect.MongoDB;
import com.enhanzer.pdf.extractor.model.template.markup.calculate.coordinates.TextDataCoordinates;
import com.enhanzer.pdf.extractor.model.template.markup.pdf.retreiver.TextPDDocument;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import org.apache.pdfbox.pdmodel.PDDocument;


import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class TextDataInserter {

    private static final String host="localhost";
    private static final String dbName="staging";
    private static final String templatesColl="templateInfo";
    private static final int    port=27017;

    public void insert(TextDataParser textDataParser) throws UnknownHostException {


            MongoDB mongoDB = new MongoDB();
            DB mongoClient = mongoDB.createMongoClient(host, dbName, port);
            DBCollection templateCollection = mongoDB.getCollection(mongoClient, templatesColl);
            BasicDBObject searchQuery = new BasicDBObject();
            /*
            Returns the DB cursor object where the mainCategory=form.mainCategory and
            subCategory=form.subCategory templateName=form.templateName dataType=form.dataType
            from the templateInfo Collections
             */
            searchQuery.put("mainCategory", textDataParser.getMainCategory());
            searchQuery.put("subCategory", textDataParser.getSubCategory());
            searchQuery.put("templateName", textDataParser.getTemplateName());
            searchQuery.put("dataType", textDataParser.getDataType());


            List<TextDataElement> textDataElements = textDataParser.getTextDataElements();

            for(TextDataElement t:textDataElements){
            DBCursor templateCursor = templateCollection.find(searchQuery);

            if (templateCursor.hasNext()) {
                // If record exists update the record
                updateRecord(textDataParser, t,templateCollection, searchQuery);
            } else {
                // If there is no record exists create a new record and input
                createNewRecord(textDataParser, t,templateCollection);
            }
            }

    }

    public void createNewRecord(TextDataParser textDataParser, TextDataElement textDataElement,DBCollection templateCollection ) {

        BasicDBObject insertObject = new BasicDBObject();
        insertObject.put("mainCategory", textDataParser.getMainCategory());
        insertObject.put("subCategory", textDataParser.getSubCategory());
        insertObject.put("templateName", textDataParser.getTemplateName());
        insertObject.put("dataType", textDataParser.getDataType());

        List<BasicDBObject> textDataElementsInsert = new ArrayList<BasicDBObject>();

        BasicDBObject textElementObject = new BasicDBObject();

        textElementObject.put("metaId", textDataElement.getMetaId());
        textElementObject.put("pageNumber", textDataElement.getPageNumber());
        textElementObject.put("pageRotation", textDataElement.getPageRotation());

        textElementObject.put("totalX1", textDataElement.getTotalX1());
        textElementObject.put("totalY1", textDataElement.getTotalY1());
        textElementObject.put("totalWidth", textDataElement.getTotalWidth());
        textElementObject.put("totalHeight", textDataElement.getTotalHeight());

        textElementObject.put("metaX1", textDataElement.getMetaX1());
        textElementObject.put("metaY1", textDataElement.getMetaY1());
        textElementObject.put("metaWidth", textDataElement.getMetaWidth());
        textElementObject.put("metaHeight", textDataElement.getMetaHeight());


        textDataElementsInsert.add(textElementObject);

        insertObject.put("textDataElements", textDataElementsInsert);

        templateCollection.insert(insertObject);

    }

    public void updateRecord(TextDataParser textDataParser,TextDataElement textDataElement,DBCollection templateCollection , BasicDBObject searchQuery ){

            BasicDBObject textElementObject = new BasicDBObject();

            textElementObject.put("metaId", textDataElement.getMetaId());
            textElementObject.put("pageNumber", textDataElement.getPageNumber());
            textElementObject.put("pageRotation", textDataElement.getPageRotation());

            textElementObject.put("totalX1", textDataElement.getTotalX1());
            textElementObject.put("totalY1", textDataElement.getTotalY1());
            textElementObject.put("totalWidth", textDataElement.getTotalWidth());
            textElementObject.put("totalHeight", textDataElement.getTotalHeight());

            textElementObject.put("metaX1", textDataElement.getMetaX1());
            textElementObject.put("metaY1", textDataElement.getMetaY1());
            textElementObject.put("metaWidth", textDataElement.getMetaWidth());
            textElementObject.put("metaHeight", textDataElement.getMetaHeight());

            BasicDBObject updateObject = new BasicDBObject();
            updateObject.put("$push", new BasicDBObject("textDataElements", textElementObject));
            templateCollection.update(searchQuery, updateObject);

        }
}
