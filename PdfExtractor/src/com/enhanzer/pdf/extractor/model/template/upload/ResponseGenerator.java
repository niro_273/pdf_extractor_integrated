package com.enhanzer.pdf.extractor.model.template.upload;

import com.enhanzer.pdf.extractor.model.data.upload.template.UploadResponse;
import com.enhanzer.pdf.extractor.model.data.upload.template.UploadStatus;
import com.google.gson.Gson;

/**
 * Created by niro273 on 8/28/14.
 */
public class ResponseGenerator {

    public String generateJsonResponse(UploadStatus uploadStatus){
        UploadResponse uploadResponse=new UploadResponse();

        uploadResponse.setMainCategory(uploadStatus.getMainCategory());
        uploadResponse.setSubCategory(uploadStatus.getSubCategory());
        uploadResponse.setTemplateName(uploadStatus.getTemplateName());
        uploadResponse.setImageRelativePaths(uploadStatus.getImageRelativePaths());

        Gson gson = new Gson();

        // convert java object to JSON format, and returned as JSON formatted string
        return gson.toJson(uploadResponse);
    }
}
