package com.enhanzer.pdf.extractor.model.template.markup;

import com.enhanzer.pdf.extractor.model.data.markup.template.MarkUpResponse;
import com.enhanzer.pdf.extractor.model.data.template.info.text.TextDataElement;
import com.enhanzer.pdf.extractor.model.data.template.info.text.TextDataParser;
import com.enhanzer.pdf.extractor.model.extractors.text.FullSelectionTextExtractor;
import com.enhanzer.pdf.extractor.model.extractors.text.MetaSelectionTextExtractor;
import com.enhanzer.pdf.extractor.model.template.markup.calculate.coordinates.TextDataCoordinates;
import com.enhanzer.pdf.extractor.model.template.markup.pdf.retreiver.TextPDDocument;
import com.google.gson.Gson;
import org.apache.pdfbox.pdmodel.PDDocument;


import java.io.IOException;
import java.util.List;


public class TextDataExtractor {

    public MarkUpResponse extractText(String jsonRequest) throws IOException {

        String extractedText;
        MarkUpResponse markUpResponse=new MarkUpResponse();

        /* Assign the input from the user to the textDataParser */
        Gson gson=new Gson();
        TextDataParser textDataParser;
        textDataParser=gson.fromJson(jsonRequest, TextDataParser.class);

        List<TextDataElement> textDataElements=textDataParser.getTextDataElements();

        TextPDDocument textPDDocument=new TextPDDocument();
        PDDocument doc=textPDDocument.retrievePDDoc(textDataParser);

        /* Get the first textDataElement because only  textDataElement to extract*/
        TextDataElement textDataElement=textDataElements.get(0);

        TextDataCoordinates textDataCoordinates=new TextDataCoordinates();
        /* Set Values for the PDF width, Height and Rotation for the first textDataElement*/
        textDataCoordinates.setPdfProperties(doc, textDataParser);
        /* Recalculate and set coordinates according to the actual pdf width and height & Page Rotation */
        textDataCoordinates.calculateCoordinates(textDataParser);

        /* If There is no metaArea defined extract the full area */
        if(textDataElement.getMetaX1()==-1) {

            FullSelectionTextExtractor fullExtractor=new FullSelectionTextExtractor();
            extractedText=fullExtractor.extract(doc,textDataElement);

        }else {
            /* If There is a metaArea defined exclude the metaArea and only extract the otherArea */
            MetaSelectionTextExtractor metaSelectionTextExtractor =new MetaSelectionTextExtractor();
            extractedText= metaSelectionTextExtractor.extractWithOutLabel(doc,textDataElement);

        }

        markUpResponse.setDataType(textDataParser.getDataType());
        markUpResponse.setExtractedData(extractedText);

        doc.close();

        return markUpResponse;
    }
}
