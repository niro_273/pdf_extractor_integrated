//Status Variable
//Global Variables
//Starting pixel coordinates of the ui component drawn upon
var uiCompStartingX;
var uiCompStartingY;

//current imgareSelection instance
var selectedImgAreInstance = undefined;


//Bufers
var currentElement = undefined;

function Page(data) {
    this.pageNumber         = ko.observable(data.pageNumber);
    this.imagePath          = ko.observable(data.imagePath);
    this.pageId             = ko.observable('Page'+this.pageNumber().toString());
    this.pageIdLi            = ko.observable('PageLi'+this.pageNumber().toString());
    this.pageIdAn           = ko.observable('PageAn'+this.pageNumber().toString());
    this.pageNumberHref     = ko.observable('#Page' + this.pageNumber().toString());
    this.pageNumberName     = ko.observable('Page '+this.pageNumber().toString());
    this.activeStatus       = ko.observable(data.activeStatus);
}

// Models
function DataElement(rectangle){
    var self = this;
    self.id = ko.observable(rectangle.id);
    self.name = ko.observable();
    self.elementId  = self.id();

    self.elementType = ko.observable(rectangle.elementType);

    self.startX = ko.observable(rectangle.startX);
    self.startY= ko.observable(rectangle.startY);
    self.width = ko.observable(rectangle.width);
    self.height = ko.observable(rectangle.height);



    self.extractedData = ko.observable(rectangle.extractedData);
    self.labelExtractedData = ko.observable(rectangle.labelExtractedData);
    //Change at message broker to meta id
    self.metaName = ko.observable(rectangle.metaName);
    self.elementClass = ko.observable('main');

    self.relevantData = ko.observable(rectangle.relevantData);
    if(rectangle.relevantData === undefined ) {
        self.relevantData = ko.observable("Select Label Element");
    }


    self.subElements = ko.observableArray([rectangle.subElements]);

    if(rectangle.subElements === undefined ) {
        self.subElements = ko.observableArray([]);
    }

    self.uiData = new UiData(rectangle);
    
}

//Relative to parent ELement!!!!
function SubDataElement(rectangle){
    var self = this;

    self.elementId  = ko.observable(rectangle.elementId);
    self.id = ko.observable(rectangle.id);
    self.elementType = ko.observable(rectangle.elementType);
    self.metaName = ko.observable();

    self.startX = ko.observable(rectangle.startX);
    self.startY= ko.observable(rectangle.startY);
    self.width = ko.observable(rectangle.width);
    self.height = ko.observable(rectangle.height);




    self.extractedData = ko.observable(rectangle.extractedData);
    self.relevantData = ko.observable(rectangle.relevantData);

    self.elementClass = ko.observable('sub');
    self.uiData = new UiData(rectangle);

}



function UiData(rectangle){
    var self = this;

    self.baseUiComponentStartX = ko.observable(rectangle.baseUiComponentStartX);
    self.baseUiComponentStartY = ko.observable(rectangle.baseUiComponentStartY);
    self.baseUiComponentHeight = ko.observable(rectangle.baseUiComponentHeight);
    self.baseUiComponentWidth = ko.observable(rectangle.baseUiComponentWidth);

    self.metaStartY = ko.computed(function(){
        return self.baseUiComponentStartY() + rectangle.startY - 97
    });

    self.metaStartX = ko.computed(function(){
        return self.baseUiComponentStartX() + rectangle.startX
    });


    self.removeX = ko.computed(function(){
        return self.baseUiComponentStartX() + rectangle.startX + rectangle.width - 14
    });


    self.removeY = ko.computed(function(){
        return self.baseUiComponentStartY() + rectangle.startY
        //margin inside di
    });

    self.rectY = ko.computed(function(){
        return self.baseUiComponentStartY() + rectangle.startY
    });
    self.rectX = ko.computed(function(){
        return self.baseUiComponentStartX() + rectangle.startX
    });

    self.extractedY = ko.computed(function(){
        return self.baseUiComponentStartY() + rectangle.startY + rectangle.height + 2
    });
    self.extractedX = ko.computed(function(){
        return self.baseUiComponentStartX() + rectangle.startX
    });

    self.elementMap = ko.computed(function(){
        var styleObject = {
            left    : self.rectX(),
            top     : self.rectY(),
            width   : rectangle.width ,
            height  : rectangle.height
        }
        return styleObject;
    });
    self.subElementMap

}

/*
function Page(data,textElements, tableElements, pictureElements, elementBuffer) {

    self.pageNumber = data.pageNumber;
    self.pictureSource = data.pictureSource;

    self.textElements = textElements;
    self.tableElements = tableElements;
    self.pictureElements = pictureElements;
    self.elementBuffer = elementBuffer;
}*/
