var ajaxExtract = function(urlData,sendData,asyncState,methodType){
    var extractedData;
    var sendData = JSON.stringify(sendData);
    $.ajax({
        url: urlData,
        data: sendData,
        type: methodType,
        async:asyncState,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json'
    }).fail(function(jqXHR, textStatus, errorThrown) {
      extractedData="ajax extraction failed textStatus: "+textStatus.toString()+"  errorThrown: "+errorThrown.toString();
      console.log(jqXHR);
      console.log(extractedData);
    })
    .done(function(data) {
      extractedData =data;
    });
    return extractedData;
}


var getMainExtraction =  function(rectangleObject, dataType){
	var ajaxReponse;
	switch (dataType){
		case 'text':
			var dataDTO = new TextDataDTO(/*Insert Cookie Data for current page template*/);
			var textDataElement = new TextDataElementDTO(rectangleObject);
			dataDTO.textDataElements.push(textDataElement);
			ajaxReponse = ajaxExtract('/MarkUpTemplateRegionController',dataDTO,false,'POST');

			break;
		case 'table':

			break;
		case 'picture':

			break;						
	}
	return ajaxReponse;
}

var getSubExtraction=  function(rectangleObject, dataType){
	var ajaxReponse; 
	var bufferedElement = ko.utils.unwrapObservable(vm.elementBuffer());
	switch (dataType){
		case 'text':
			var dataDTO = new TextDataDTO(/*Insert Cookie Data for current page template*/);
			var textDataElement = new TextDataElementDTO(bufferedElement,rectangleObject);
			dataDTO.textDataElements.push(textDataElement);
			ajaxReponse = ajaxExtract('/MarkUpTemplateRegionController',dataDTO,false,'POST');

			break;
		case 'table':

			break;
		case 'picture':

			break;	
	}
	return ajaxReponse;
}






function TextDataDTO(pageData, dataType){
    this.mainCategory="Sales Order";////////////pageData.mainCategory;/////////???????GetFromCookie 
    this.subCategory=  "Supplier 10";////////////pageData.subCategory;/////////???????GetFromCookie 
    this.templateName=  "template1";//////////// pageData;/////////???????GetFromCookie 
    this.status= "extract";///Static Data
    this.dataType= "text";////Static Data    
    this.textDataElements=[];
}
function TextDataElementDTO(dataElement, metaElement){
    this.metaId = ko.utils.unwrapObservable(dataElement.id);
    this.totalX1 =ko.utils.unwrapObservable(dataElement.startX);
    this.totalY1 =ko.utils.unwrapObservable(dataElement.startY);
    this.totalWidth = this.totalX1 + ko.utils.unwrapObservable(dataElement.width);
    this.totalHeight = this.totalY1 + ko.utils.unwrapObservable(dataElement.height);
    this.pageNumber = 1;/////////???????GetFromCookie 

    this.metaX1 =-1;
    this.metaY1 =-1; 
    this.metaWidth =-1;   
    this.metaHeight =-1;    

    if(metaElement !== undefined){
	    this.metaX1 =this.totalX1 + ko.utils.unwrapObservable(metaElement.startX);
	    this.metaY1 =this.totalY1 + ko.utils.unwrapObservable(metaElement.startY);
	    this.metaWidth =this.metaX1 +  ko.utils.unwrapObservable(metaElement.width);
	    this.metaHeight = this.metaY1 + ko.utils.unwrapObservable(metaElement.height);
    }

 }












function TableDataDTO(pageData, tableDataElements){
    var rtnDTO;
  	this.mainCategory="Sales Order";
  	this.subCategory= "Supplier 10";
  	this.templateName= "template1";
  	this.status= "extract";
  	this.dataType= "table";    
  	this.tableDataElements=[];
}
function PictureDataDTO(pageData, pictureDataELements){
    var rtnDTO;
    this.mainCategory="Sales Order";
    this.subCategory= "Supplier 10";
    this.templateName= "template1";
    this.status= "extract";
    this.dataType= "text";    
    this.pictureDataELements=[];
}















function DataDTO(pageData , textDataElements, tableDataElements, imageDataElements){
    var rtnDTO;
    rtnDTO.mainCategory="Sales Order";
    rtnDTO.subCategory= "Supplier 10";
    rtnDTO.templateName= "template1";
    rtnDTO.status= "extract";

    rtnDTO.textDataElements=[];
    rtnDTO.tableDataElements=[];
    rtnDTO.imageDataElements=[];
}
