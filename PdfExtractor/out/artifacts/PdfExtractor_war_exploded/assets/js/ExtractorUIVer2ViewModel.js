function ViewModel(){
    
    var self = this;
    self.currentImage = ko.observable('assets/img/pdfimage1.jpg');

    self.selectionInProgress = ko.observable(false);
    self.subElementSelectionInProgress = ko.observable(false);
    self.currentSelection = ko.observable();

    self.currentSelectionsBuffer= ko.observableArray([]);
    self.textElements = ko.observableArray([]);
    self.tableElements = ko.observableArray([]);
    self.pictureElements = ko.observableArray([]);
    self.tempSubs = ko.observableArray([]);
    self.dataElements = ko.computed(function(){
        elements = ko.observableArray([]);
        for(var key in self.textElements()){
            elements.push(self.textElements()[key]);
        }
        for(var key in self.tableElements()){
            elements.push(self.tableElements()[key]);
        }
        for(var key in self.pictureElements()){
            elements.push(self.pictureElements()[key]);
        }
        return elements;


    });

    self.subDataElements = ko.computed(function(){
        elements = ko.observableArray([]);
        for(var key in self.textElements()){
            for(var innerKey in self.textElements()[key].subElements()){
                elements.push(self.textElements()[key].subElements()[innerKey]);
            }
        }
        for(var key in self.tableElements()){
            for(var innerKey in self.tableElements()[key].subElements()){
                elements.push(self.tableElements()[key].subElements()[innerKey]);
            }
        }
        for(var key in self.pictureElements()){
            for(var innerKey in self.pictureElements()[key].subElements()){
                elements.push(self.pictureElements()[key].subElements()[innerKey]);
            }
        }
        return elements;
    });

    self.elementBuffer = ko.observable();

    self.test = function(data){

        alert(data);
        console.log(data);
    }

    self.addTextElement = function (data){
        var element = new DataElement(data);
        self.elementBuffer(element);
        self.textElements.push(element);
    }
    self.addPictureElement = function (data){
        var element = new DataElement(data);
        self.elementBuffer(element);
        self.pictureElements.push(element);

    }
    self.addTableElement = function (data){
        var element = new DataElement(data);
        self.elementBuffer(element);
        self.tableElements.push(element);

    }

    self.addSubElement = function (data){
        var subElement = new SubDataElement(data);
        self.elementBuffer().subElements.push(subElement);

        if (data.elementType === 'text') {
            //Remove element
            var relevantTextElement  = self.textElements.remove(function(item) { 
                return item.elementId === data.elementId;
            })[0];
            relevantTextElement.relevantData(subElement.relevantData());
            relevantTextElement.subElements.push(subElement);
            self.textElements.push(relevantTextElement);
        }
        else if (data.elementType === 'table') {
            var relevantTableElement  = self.tableElements.remove(function(item) { 
                return item.elementId === data.elementId;
            })[0];
            relevantTableElement.relevantData(subElement.relevantData());
            relevantTableElement.subElements.push(subElement);
            self.tableElements.push(relevantTableElement);
        }
        else if (data.elementType === 'picture') {
            var relevantPictureElement  = self.pictureElements.remove(function(item) { 
                return item.elementId === data.elementId;
            })[0];
            relevantPictureElement.relevantData(subElement.relevantData());
            relevantPictureElement.subElements.push(subElement);
            self.pictureElements.push(relevantPictureElement);
        }
        self.tempSubs.push(subElement);
            ////////////////
            ////////////////
    }


    self.removeElement = function (removedElement){
        if(removedElement.elementClass() === 'main'){
            self.elementBuffer = ko.observable();
            if (removedElement.elementType() === 'text') {
                //Remove element
                self.textElements.remove(removedElement);
            }
            else if (removedElement.elementType() === 'table') {
                self.tableElements.remove(removedElement);
            }
            else if (removedElement.elementType() === 'picture') {
                self.pictureElements.remove(removedElement);
            }
            resetEnvironment();
        }
        else if(removedElement.elementClass() === 'sub'){
            self.elementBuffer().subElements.remove(removedElement);
            if (data.elementType === 'text') {
                //Remove element
                var relevantTextElement  = self.textElements.remove(function(item) { 
                    return item.elementId === data.elementId;
                })[0];

                relevantTextElement.subElements.remove(removedElement);
                self.textElements.push(relevantTextElement);
            }
            else if (data.elementType === 'table') {
                var relevantTableElement  = self.tableElements.remove(function(item) { 
                    return item.elementId === data.elementId;
                })[0];

                relevantTableElement.subElements.remove(removedElement);
                self.tableElements.push(relevantTableElement);
            }
            else if (data.elementType === 'picture') {
                var relevantPictureElement  = self.pictureElements.remove(function(item) { 
                    return item.elementId === data.elementId;
                })[0];

                relevantPictureElement.subElements.remove(removedElement);
                self.pictureElements.push(relevantPictureElement);
            }
        }

        
    }

    self.saveSelection = function(element,selection){
        elementBuffer = ko.observable();
    }
    self.cancelSelection = function(){
        if (self.elementBuffer() !== undefined){
            if (self.elementBuffer().elementType() === 'text') {
            //Remove element
            self.textElements.remove(self.elementBuffer());
            }
            else if (self.elementBuffer().elementType() === 'table') {
                self.tableElements.remove(self.elementBuffer());
            }
            else if (self.elementBuffer().elementType() === 'picture') {
                self.pictureElements.remove(self.elementBuffer());
            }
            elementBuffer = ko.observable();
        }
    }

    self.sendingJson = ko.observable("Content Creating");

    self.sendJson = function (){
        var data = {};
        data.textElements   =   self.textElements();
        data.tableElements   =   self.tableElements();
        data.pictureElements   =   self.pictureElements();
        self.sendingJson(ko.toJSON(data));    
    }


}

var vm = new ViewModel();
ko.applyBindings(vm);