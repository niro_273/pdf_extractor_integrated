<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Mark Up Regions</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/imgareaselect-default.css" />

    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.imgareaselect.pack.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <style>
        span {
            word-wrap:break-word;
        }

        .bs-callout {
            padding: 5px;
            margin: 12px;
            border: 1px solid #eee;
            border-left-width: 5px;
            border-radius: 3px;
            border-left-color: #5bc0de;
            margin-top: -5px;
            background-color: #fff;
        }
        .bs-callout h4 {
            margin: 5 5 0 5px;

            color: #5bc0de;
        }
        .bs-callout p{
            margin: 5 5 5px;
        }

        .bs-callout p:last-child {
            margin-bottom: 0;
        }

        .bs-docs-section{
            position:absolute;
            background-color:#fff ;
            border:1px solid #eee;
            padding-top:20px;
            border-radius: 10px;

        }

        img{
            margin: 70 6 5 5;
            border: 1px solid #eee;
            max-width: 100%;

        }




    </style>

</head>
<body>
<nav class="navbar navbar-default" style="position:fixed; width:100%; z-index: 2;" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        &nbsp;
        <div id="elementTypeSelector" class="btn-group btn-group-lg" style="padding:5 5 0 0px; margin: 0 0 5 0px">
            <button id ="textSelect"    type="button" class="btn btn-default">Text</button>
            <button id ="tableSelect"    type="button" class="btn btn-default">Table</button>
            <button id ="pictureSelect"  type="button" class="btn btn-default">Picture</button>
        </div>
        <button id ="cancelSelection" style="width:75px; margin: 0 5 0 0px; height: 40" type="button" class="btn btn-danger  btn-default">Cancel</button>
        <button id ="saveSelection" style=" margin: 0 5 0 0px; height: 40" type="button" class="btn btn-warning  btn-default">Save Element</button>
    </div>


    <!--CENTRE-->
    <div  href="#"><span style="margin:0 0 0 250px; font-size:25px" id="runningInstructions">Select Element Type</span></div>
    <div class="navbar-right">
        <button data-bind="click:sendJson" id ="persist" style="margin: -28 10 5 5px; height: 40" type="button" class="btn btn-success btn-default"><strong>Save Template</strong></button>
    </div>
</nav>
<br>
<br>
<br>
<div id="Map">
    Starting Coordinate <span id="starting"></span>
    <br>
    Ending Coordinate <span id="ending"></span>
    <br>
</div>
<div data-bind="template:{name:'rectangleTemplate', foreach:dataElements()() }"></div>
<div data-bind="template:{name:'subRectangleTemplate', foreach:subDataElements()() }"></div>


<img style="margin-top:10px" id="templatingImage" data-bind="attr:{src: currentImage}"/>



<p data-bind="text:sendingJson"></p>

<script type="text/html" id="rectangleTemplate">

    <div class="mainElement" style="position:absolute; border-style:solid; border-color:#2980b9; border-width: 3px;" data-bind="style:uiData.elementMap(), id:id"></div>

    <button  style="position:absolute; margin-top:-11; height:24; width:25; border-radius: 50px; z-index:1" data-bind="id:(id()+'RemoveButton'), click:$parent.removeElement, style:{left: uiData.removeX, top:uiData.removeY}" type="button" class="btn btn-default btn-xs removeElement">
        <span class="glyphicon glyphicon-remove-circle"></span>
    </button>

    <div class="bs-docs-section elementDecoExtracted" data-bind="style:{top:uiData.extractedY, left:uiData.extractedX, maxWidth:width}, id:id" >
        <div class="bs-callout">

            <h4>Extracted Total Data: </h4>
            <p data-bind="text:extractedData">
                The navbar collapses into its vertical mobile view when the viewport is narrower than @grid-float-breakpoint, and expands into its horizontal non-mobile view when the viewport is at least @grid-float-breakpoint in width. Adjust this variable in the Less source to control when the navbar collapses/expands. The default value is 768px (the smallest "small" or "tablet" screen)
            </p>
        </div>
        <div class="bs-callout" >
            <h4>Extracted Filtered Data: </h4>
            <p data-bind="text:relevantData">
                The navbar collapses into its vertical mobile view when the viewport is narrower than @grid-float-breakpoint, and expands into its horizontal non-mobile view when the viewport is at least @grid-float-breakpoint in width. Adjust this variable in the Less source to control when the navbar collapses/expands. The default value is 768px (the smallest "small" or "tablet" screen)
            </p>

        </div>
    </div>



</script>

<script type="text/html" id="subRectangleTemplate">
    <div class="subElement" style="position:absolute; border-style:solid; border-color:#2980b9; border-width: 3px;" data-bind="style:uiData.elementMap(), id:id">

    </div>
    <button  style="position:absolute;visibility:visible; margin-top:-11; height:24; width:25; border-radius: 50px" data-bind="id:(id()+'RemoveButton'), click:$parent.removeElement, style:{left: uiData.removeX, top:uiData.removeY}" type="button" class="btn btn-default btn-xs removeSubElement">
        <span class="glyphicon glyphicon-remove-circle"></span>
    </button>

</script>




<script type="text/javascript" src="assets/js/knockout-3.2.0.js" ></script>
<script type="text/javascript" src="assets/js/ExtractorUIVerFunctions.js"> </script>
<script type="text/javascript" src="assets/js/ExtractorUiVer2Models.js"> </script>
<script type="text/javascript" src="assets/js/ExtractorUIVer2ViewModel.js"> </script>
<script type="text/javascript" src="assets/js/messsageBroker.js"> </script>

</body>


</html>
