<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <!-------------------------------- CSS Files------------------------------------>
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">

    <!-------------------------------- JS Files------------------------------------>
    <script type="text/javascript" src="assets/js/jquery-1.8.2.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>

    <script>
        var isMCValid = true;
        window.onload = assignSelectOptions;
        function assignSelectOptions() {
            var data={ request : "getMainCategories"};
            $.ajax({
                type: "POST",
                url: "FormPopulateController",
                data: JSON.stringify(data),
                success: function(data, textStatus, jqXHR) {
                    var messages = JSON.parse(jqXHR.responseText);
                    $('#showMC').empty().append('<option value="" disabled selected> Select a Main Category </option>');
                    for(var i=0;i<messages.mainCategories.length;i++){
                        $('#showMC').append('<option value="' + messages.mainCategories[i] + '">' + messages.mainCategories[i] + '</option>');
                    }
                }
            });
        }

        function getSC() {
            var dropDownMC = document.getElementById("showMC");
            var dbMCName = dropDownMC.options[dropDownMC.selectedIndex].value;

            var CreateSCData = { request: "getSubCategories",
                selectedMC: dbMCName };
            $.ajax({
                type: 'POST', url: 'FormPopulateController',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify(CreateSCData),
                success: function (data, textStatus, jqXHR) {
                    var messages = JSON.parse(jqXHR.responseText);
                    $('#showSC').empty().append('<option value="" disabled selected> Select a Sub Category </option>');
                    for(var i=0;i<messages.subCategories.length;i++){
                        $('#showSC').append('<option value="' + messages.subCategories[i] + '">' + messages.subCategories[i] + '</option>');
                    }
                }
            });
        }

        function getTemplates() {

            var dropDownMC = document.getElementById("showMC");
            var dbMCName = dropDownMC.options[dropDownMC.selectedIndex].value;

            var dropDownSC = document.getElementById("showSC");
            var dbSCName = dropDownSC.options[dropDownSC.selectedIndex].value;

            var CreateSCData = { request: "getTemplates",
                selectedMC: dbMCName,
                selectedSC: dbSCName};
            $.ajax({
                type: 'POST', url: 'FormPopulateController',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify(CreateSCData),
                success: function (data, textStatus, jqXHR) {
                    var messages = JSON.parse(jqXHR.responseText);

                    $('#showTemplates').empty().append('<option value="" disabled selected> Select a Template</option>');

                    for(var i=0;i<messages.templateNames.length;i++){
                        $('#showTemplates').append('<option value="' + messages.templateNames[i] + '">' + messages.templateNames[i] + '</option>');
                    }
                }
            });
        }


    </script>
</head>
<body>

<h1>File Upload Form</h1>

<fieldset>
    <legend>Upload File</legend>
    <form >


        <label for="showMC">Select a Main Category</label>
        <select id="showMC" name="showMC" onchange="getSC(this)">
        </select>   </br>

        <label for="showSC">Select a Sub Category</label>
        <select id="showSC" name="showSC" onchange="getTemplates(this)">
        </select>   </br>

        <label for="showTemplates">Select A Template</label>
        <select id="showTemplates" name="showTemplates" >
        </select>   </br>

        <label for="documentId">Type the Document ID</label>
        <input id="documentId" name="documentId" type="text" value="doc1"/> <br/>

        <label for="pdfFile">Select File: </label>
        <input id="pdfFile" type="file" name="pdfFile" size="30" required/><br/>

        <input type="button" value="Upload" onclick="upload()"/>
    </form>
</fieldset>

<script>
    var client = new XMLHttpRequest();
    function upload()
    {
        var dropDownMC = document.getElementById("showMC");
        var mainCategory = dropDownMC.options[dropDownMC.selectedIndex].value;
        /* alert the user to select a main category before upload  */
        if(mainCategory==""){
            alert("Select a Main Category");
            return false;
        }

        var dropDownSC = document.getElementById("showSC");
        var subCategory = dropDownSC.options[dropDownSC.selectedIndex].value;
        /* alert the user to select a subcategory before upload */
        if(subCategory=="" || subCategory=="null"){
            alert("Select a Sub Category");
            return false;
        }

        var dropDownTemp = document.getElementById("showTemplates");
        var templateName = dropDownTemp.options[dropDownTemp.selectedIndex].value;
        /* alert the user to select a subcategory before upload */
        if(templateName=="" || templateName=="null"){
            alert("Select a Template");
            return false;
        }

        //var templateName = document.getElementById("templateName").value;
        var documentId = document.getElementById("documentId").value;
        var file = document.getElementById("pdfFile");

        /* alert the user to input a value to the Document ID*/
        if(documentId==""){
            alert("Document ID is required");
            return false;
        }

        var fileName = $("#pdfFile").val();
        /* alert the user to select a File */
        if(fileName==""){
            alert("Select a PDF File");
            return false;
        }

        /* Create a FormData instance */
        var formData = new FormData();
        /* Add the file */
        formData.append("mainCategory", mainCategory);
        formData.append("subCategory", subCategory);
        formData.append("templateName", templateName);
        formData.append("documentId", documentId);
        formData.append("pdfFile", file.files[0]);

        client.open("post", "/ExtractPdfController", true);
        client.send(formData);  /* Send to server */
    }

    client.onload = function() {
        if (client.status == 200) {
            if(client.responseText=="success") {
                var successUrl = "MarkUpTemplateRegions.jsp";
                window.location.href = successUrl;
            }else{
                alert(client.responseText);
            }
        }
    }
</script>

</body>
</html>
