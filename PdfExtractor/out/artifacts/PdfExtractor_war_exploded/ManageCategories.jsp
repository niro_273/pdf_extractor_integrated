<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <!-------------------------------- CSS Files------------------------------------>
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">

    <!-------------------------------- JS Files------------------------------------>
    <script type="text/javascript" src="assets/js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>

    <script>
        var isMCValid = true;
        window.onload = assignSelectOptions;
        function assignSelectOptions() {
            var data={ request : "getMainCategories"};
            $.ajax({
                type: "POST",
                url: "FormPopulateController",
                data: JSON.stringify(data),
                success: function(data, textStatus, jqXHR) {
                    var messages = JSON.parse(jqXHR.responseText);
                    $('#getMainCategory').empty().append('<option value="" disabled selected> Select a Main Category </option>');
                    for(var i=0;i<messages.mainCategories.length;i++){
                        $('#getMainCategory').append('<option value="' + messages.mainCategories[i] + '">' + messages.mainCategories[i] + '</option>');
                    }

                    $('#showMC').empty();
                    var mcDropDown = document.getElementById("showMC");
                    mcDropDown.size = messages.mainCategories.length;
                    for(var i=0;i<messages.mainCategories.length;i++){
                        $('#showMC').append('<option value="' + messages.mainCategories[i] + '">' + messages.mainCategories[i] + '</option>');
                    }

                }
            });
        }
    </script>
</head>
<body>
<div class="row">
    <div class="col-md-12">
        Div Tag For Navigation

    </div>

    <div class="col-md-12">
        Div tag to Create a New Main Category
        <form>
            <label for="createMainCategory">Main Category</label>
            <input id="createMainCategory" type="text" name="createMainCategory" placeholder="Main Category Name" required />
            <input type="button" value="create" onclick="createMC()"/>
        </form>

    </div>

    <div class="col-md-12">
        Div tag to Create a New Sub CateGory
        <form >
            <select id="getMainCategory" name="getMainCategory" required>
                <option value="" disabled selected>Select a Main Category</option>
            </select>

            <label for="createSubCategory">Sub Category</label>
            <input id="createSubCategory" type="text" name="createSubCategory" placeholder="Sub Category Name" required />
            <input type="button" value="create" onclick="createSC()"/>

        </form>
    </div>

    <div class="col-md-12">
        Div tag to View All The Main and Relevant Sub Categories and TemplateNames
        <form >
            <select id="showMC" name="showMC" onchange="getSC(this)">
            </select>

            <select id="showSC" name="showSC" onchange="getTemplates(this)">
            </select>

            <select id="showTemplates" name="showTemplates" >
            </select>

        </form>
    </div>

    <script>
        function createMC() {
            var mcName= document.getElementById("createMainCategory").value;
            var CreateMCData={ request : "createMainCategory",
                        mainCategory: mcName};

            if(mcName==""){
                alert("Main Category Name is Required");
            }

            $.ajax({
                type: 'POST', url: 'ManageCategoriesController',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify(CreateMCData),
                success: function(data, textStatus, jqXHR) {
                    var messages = JSON.parse(jqXHR.responseText);
                    assignSelectOptions();
                    alert(messages.statusMessage);
                }
            });

        }

        function createSC() {

            var dropDownMC = document.getElementById("getMainCategory");
            var dbMCName = dropDownMC.options[dropDownMC.selectedIndex].value;

            if(dbMCName==""){
                alert("Select a Category");

            }else {

                var scName = document.getElementById("createSubCategory").value;
                var CreateSCData = { request: "createSubCategory",
                    mainCategory:dbMCName,
                    subCategory: scName};
                $.ajax({
                    type: 'POST', url: 'ManageCategoriesController',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    data: JSON.stringify(CreateSCData),
                    success: function (data, textStatus, jqXHR) {
                        var messages = JSON.parse(jqXHR.responseText);
                        assignSelectOptions();
                        alert(messages.statusMessage);
                    }
                });
            }

        }

        function getSC() {

            var dropDownMC = document.getElementById("showMC");
            var dbMCName = dropDownMC.options[dropDownMC.selectedIndex].value;

            var CreateSCData = { request: "getSubCategories",
                selectedMC: dbMCName };
            $.ajax({
                type: 'POST', url: 'FormPopulateController',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify(CreateSCData),
                success: function (data, textStatus, jqXHR) {
                    var messages = JSON.parse(jqXHR.responseText);

                    $('#showSC').empty();
                    var scDropDown = document.getElementById("showSC");
                    scDropDown.size = messages.subCategories.length;

                    for(var i=0;i<messages.subCategories.length;i++){
                        $('#showSC').append('<option value="' + messages.subCategories[i] + '">' + messages.subCategories[i] + '</option>');
                    }
                }
            });


        }

        function getTemplates() {

            var dropDownMC = document.getElementById("showMC");
            var dbMCName = dropDownMC.options[dropDownMC.selectedIndex].value;

            var dropDownSC = document.getElementById("showSC");
            var dbSCName = dropDownSC.options[dropDownSC.selectedIndex].value;

            var CreateSCData = { request: "getTemplates",
                                selectedMC: dbMCName,
                                selectedSC: dbSCName};
            $.ajax({
                type: 'POST', url: 'FormPopulateController',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify(CreateSCData),
                success: function (data, textStatus, jqXHR) {
                    var messages = JSON.parse(jqXHR.responseText);

                    $('#showTemplates').empty();

                    var tempDropDown = document.getElementById("showTemplates");
                    tempDropDown.size = messages.templateNames.length;

                    for(var i=0;i<messages.templateNames.length;i++){
                        $('#showTemplates').append('<option value="' + messages.templateNames[i] + '">' + messages.templateNames[i] + '</option>');
                    }
                }
            });


        }
    </script>

</div>
</body>
</html>
